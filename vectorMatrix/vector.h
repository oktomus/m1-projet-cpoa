#ifndef __VECTOR__
#define __VECTOR__

#include "array.h"
#include <stdexcept>
#include <cmath>

/*
                        ▄                 
 ▄   ▄   ▄▄▄    ▄▄▄   ▄▄█▄▄   ▄▄▄    ▄ ▄▄ 
 ▀▄ ▄▀  █▀  █  █▀  ▀    █    █▀ ▀█   █▀  ▀
  █▄█   █▀▀▀▀  █        █    █   █   █    
   █    ▀█▄▄▀  ▀█▄▄▀    ▀▄▄  ▀█▄█▀   █    
                                          
*/
                                          
//////////////////////////////////////
/// Definition
//////////////////////////////////////

/**
  * A representation of a vector of any dimension
  */
template <typename T, size_t S>
class Vector : public Array<T, S>
{

public:

    /**
      * Inherits all constructors from array
      */
    using Array<T, S>::Array;

    /**
     * @brief Calculate the distance between two vectors with squareing the result
     * @param b
     * @return
     */
    T distanceSquaredTo(const Vector& b) const;

    /**
     * @brief distanceTo, use sqrt(distanceSquaredTo())
     * @param b
     * @return
     */
    T distanceTo(const Vector& b) const;

    /**
      * Addition with another vector
      *
      * @param b    The other vector
      * @return     The result vector
      */
    Vector& operator+= (const Vector& b);
    template <typename T2, size_t S2>
    friend Vector<T2, S2> operator +(Vector<T2, S2> a, const Vector<T2, S2>& b);

    /**
      * Substraction with another vector
      *
      * @param b    The other vector
      * @return     The result vector
      */
    Vector& operator-= (const Vector& b);
    template <typename T2, size_t S2>
    friend Vector<T2, S2> operator-(Vector<T2, S2> a, const Vector<T2, S2>& b);

    /**
      * Multiply the vector with a scalar
      *
      * @param b    The scalar
      * @return     The vector multiplied
      */
    Vector& operator*= (const T& b);
    template <typename T2, size_t S2>
    friend Vector<T2, S2> operator* (Vector<T2, S2> a, const T2& b);
    template <typename T2, size_t S2>
    friend Vector<T2, S2> operator* (const T2& b, Vector<T2, S2> a);

    /**
      * Calculate the dot product of two vectors
      *
      * @param b    The other vector
      * @return     A scalar 
      */
    T operator*= (const Vector<T, S>& b);
    template <typename T2, size_t S2>
    friend T2 operator* (const Vector<T2, S2> & a, const Vector<T2, S2> & b);

    /**
      * Cross product between two vectors the vector with a scalar
      *
      * @param b    The other vector
      * @return     The cross result
      */
    Vector& operator^= (const Vector<T, S>& b);
    template <typename T2, size_t S2>
    friend Vector<T2, S2> operator^ (Vector<T2, S2> a, const Vector<T2, S2>& b);


    /**
      * Divide the vector by a scalar
      *
      * @param b    The scalar
      * @return     The divided scalar
      */
    Vector& operator/= (const T& b);
    template <typename T2, size_t S2>
    friend Vector<T2, S2> operator/ (Vector<T2, S2> a, const T2& b);

    /**
      * Convert the vector into an output stream
      */
    template <typename T2, size_t S2>
    friend std::ostream& operator<< (std::ostream& a, const Vector<T2, S2>& b);
};

//////////////////////////////////////
/// Types
//////////////////////////////////////

using Vec2f = Vector<float, 2>;
using Vec3f = Vector<float, 3>;
using Vec4f = Vector<float, 4>;
using Vec2i = Vector<int, 2>;
using Vec3i = Vector<int, 3>;
using Vec4i = Vector<int, 4>;
using Vec2uc = Vector<unsigned char, 2>;
using Vec3uc = Vector<unsigned char, 3>;
using Vec4uc = Vector<unsigned char, 4>;

//////////////////////////////////////
/// Implementation
//////////////////////////////////////

template <typename T, size_t S>
T Vector<T, S>::distanceSquaredTo(const Vector<T, S>& b) const
{
    T res(0);
    for(size_t i = 0; i < S; ++i)
    {
        res += pow((*this)[i] - b[i], 2);
    }
    return res;
}

template <typename T, size_t S>
T Vector<T, S>::distanceTo(const Vector<T, S>& b) const
{
    return sqrt(distanceSquaredTo(b));
}

template <typename T, size_t S>
Vector<T, S>& Vector<T, S>::operator+= (const Vector<T, S>& b)
{
    for(size_t i = 0; i < S; ++i)
    {
        (*this)[i] += b[i];
    }
    return *this;
}

template <typename T, size_t S>
Vector<T, S> operator +(Vector<T, S> a, const Vector<T, S>& b)
{
    a += b;
    return a;
}

template <typename T, size_t S>
Vector<T, S>& Vector<T, S>::operator-= (const Vector<T, S>& b)
{
    for(size_t i = 0; i < S; ++i)
    {
        (*this)[i] -= b[i];
    }
    return *this;
}

template <typename T, size_t S>
Vector<T, S> operator-(Vector<T, S> a, const Vector<T, S>& b)
{
    a -= b;
    return a;
}

template <typename T, size_t S>
Vector<T, S>&Vector<T, S>::operator*= (const T& b)
{
    for(size_t i = 0; i < S; ++i)
    {
        (*this)[i] *= b;
    }
    return *this;
}

template <typename T, size_t S>
Vector<T, S>operator* (Vector<T, S>a, const T& b)
{
    a *= b;
    return a;
}

template <typename T, size_t S>
Vector<T, S>operator* (const T& b, Vector<T, S>a)
{
    a *= b;
    return a;
}

template <typename T, size_t S>
T Vector<T, S>::operator*= (const Vector<T, S>& b)
{
    return (*this) * b;
}

template <typename T, size_t S>
T operator* (const Vector<T, S> & a, const Vector<T, S> & b)
{
    T res(0);

    for(size_t i = 0; i < S; ++i)
    {
        res += a[i] * b[i];
    }

    return res;
}

template <typename T, size_t S>
Vector<T, S>& Vector<T, S>::operator^=(const Vector<T, S>& b)
{
    if (S != 3)
    {
        throw std::invalid_argument("Cross products works only on 3 dimensions vectors");
    }
    Vector<T, S> a(*this);

    (*this)[0] = a[1] * b[2] - a[2] * b[1];
    (*this)[1] = a[2] * b[0] - a[0] * b[2];
    (*this)[2] = a[0] * b[1] - a[1] * b[0];

    return *this;
}

template <typename T, size_t S>
Vector<T, S> operator^ (Vector<T, S>a, const Vector<T, S>& b)
{
    a ^= b;
    return a;
}

template <typename T, size_t S>
Vector<T, S>&Vector<T, S>::operator/= (const T& b)
{
    if (b == T(0))
    {
        throw std::overflow_error("Division by Zero not allowed");
    }

    for(size_t i = 0; i < S; ++i)
    {
        (*this)[i] /= b;
    }
    return *this;
}

template <typename T, size_t S>
Vector<T, S>operator/ (Vector<T, S>a, const T& b)
{
    a /= b;
    return a;
}
template <typename T, size_t S>
std::ostream& operator<< (std::ostream& a, const Vector<T, S>& b)
{
    a << "(";
    for(size_t i = 0; i < S; ++i)
    {
        a << b[i];
        if (i < S-1) a << ", ";
    }
    a << ")";
    return a;
}


#endif
