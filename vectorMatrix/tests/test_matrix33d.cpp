#include "matrix33d.h"
#include "vector.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <chrono>
#include <iostream>

using namespace std::chrono;

TEST(Matrix33d, testIdentity)
{
    Matrix33d m1 = Matrix33d::identity();

    ASSERT_DOUBLE_EQ(m1[0][0], 1);
    ASSERT_DOUBLE_EQ(m1[0][1], 0);
    ASSERT_DOUBLE_EQ(m1[0][2], 0);
    ASSERT_DOUBLE_EQ(m1[1][0], 0);
    ASSERT_DOUBLE_EQ(m1[1][1], 1);
    ASSERT_DOUBLE_EQ(m1[1][2], 0);
    ASSERT_DOUBLE_EQ(m1[2][0], 0);
    ASSERT_DOUBLE_EQ(m1[2][1], 0);
    ASSERT_DOUBLE_EQ(m1[2][2], 1);
}

TEST(Matrix33d, testDefault)
{
    Matrix33d m1;

    ASSERT_DOUBLE_EQ(m1[0][0], 1);
    ASSERT_DOUBLE_EQ(m1[0][1], 0);
    ASSERT_DOUBLE_EQ(m1[0][2], 0);
    ASSERT_DOUBLE_EQ(m1[1][0], 0);
    ASSERT_DOUBLE_EQ(m1[1][1], 1);
    ASSERT_DOUBLE_EQ(m1[1][2], 0);
    ASSERT_DOUBLE_EQ(m1[2][0], 0);
    ASSERT_DOUBLE_EQ(m1[2][1], 0);
    ASSERT_DOUBLE_EQ(m1[2][2], 1);
}

TEST(Matrix33d, testAssign)
{
    Matrix33d m1;

    m1[0][2] = 100.2;

    ASSERT_DOUBLE_EQ(m1[0][2], 100.2);
}

TEST(Matrix33d, testEqual)
{
    Matrix33d m1;
    Matrix33d m2;

    ASSERT_TRUE(m1 == m2);
    ASSERT_FALSE(m1 != m2);

    m1[0][0] = 500.12;
    m2[0][0] = 500.12;

    ASSERT_TRUE(m1 == m2);
    ASSERT_FALSE(m1 != m2);
}

TEST(Matrix33d, testEqualCopy)
{
    Matrix33d m1;
    m1[0][0] = 500.12;
    Matrix33d m2(m1);

    ASSERT_TRUE(m1 == m2);
    ASSERT_FALSE(m1 != m2);

    m1[0][0] = 600.12;

    ASSERT_TRUE(m1 != m2);
    ASSERT_FALSE(m1 == m2);
}

TEST(Matrix33d, testNotEqual)
{
    Matrix33d m1;
    Matrix33d m2;

    m1[0][0] = 500.12;

    ASSERT_TRUE(m1 != m2);
    ASSERT_FALSE(m1 == m2);

}

TEST(Matrix33d, testInverseIdentity)
{
    Matrix33d m1;
    Matrix33d result;
    result[0][2] = 90;
    int code;

    code = m1.invert(result);

    ASSERT_EQ(code, 0);
    ASSERT_TRUE(result == m1);
}

TEST(Matrix33d, testInvertTransforms)
{
    Matrix33d m1 = Matrix33d::identity();
    m1.translate(3.5f,2.4f);
    m1.rotate(0.4);
    Matrix33d m2;
    ASSERT_EQ(0, m1.invert(m2));
    ASSERT_TRUE(m1 * m2 == Matrix33d::identity());
}

TEST(Matrix33d, testInverse)
{
    Matrix33d m1;

    m1[0][0] = 2;
    m1[0][1] = 3;
    m1[0][2] = 8;

    m1[1][0] = 6;
    m1[1][1] = 0;
    m1[1][2] = -3;

    m1[2][0] = -1;
    m1[2][1] = 3;
    m1[2][2] = 2;

    Matrix33d result;
    int code;

    code = m1.invert(result);

    ASSERT_EQ(code, 0);
    ASSERT_DOUBLE_EQ(result[0][0], 1.0/15);
    ASSERT_DOUBLE_EQ(result[0][1], 2.0/15);
    ASSERT_DOUBLE_EQ(result[0][2], -1.0/15);
    ASSERT_DOUBLE_EQ(result[1][0], -1.0/15);
    ASSERT_DOUBLE_EQ(result[1][1], 4.0/45);
    ASSERT_DOUBLE_EQ(result[1][2], 2.0/5);
    ASSERT_DOUBLE_EQ(result[2][0], 2.0/15);
    ASSERT_DOUBLE_EQ(result[2][1], -1.0/15);
    ASSERT_DOUBLE_EQ(result[2][2], -2.0/15);
}

TEST(Matrix33d, testInverseFail)
{
    Matrix33d m1;

    m1[0][0] = 2;
    m1[0][1] = 3;
    m1[0][2] = 8;

    m1[1][0] = 0;
    m1[1][1] = 0;
    m1[1][2] = 0;

    m1[2][0] = -1;
    m1[2][1] = 3;
    m1[2][2] = 2;

    Matrix33d result;
    int code;

    code = m1.invert(result);

    ASSERT_NE(code, 0);
}

TEST(Matrix33d, testTranslateInit)
{
    Matrix33d translate = Matrix33d::translation(10, -5);

    ASSERT_DOUBLE_EQ(translate[0][0],1);
    ASSERT_DOUBLE_EQ(translate[0][1],0);
    ASSERT_DOUBLE_EQ(translate[0][2],10);
    ASSERT_DOUBLE_EQ(translate[1][0],0);
    ASSERT_DOUBLE_EQ(translate[1][1],1);
    ASSERT_DOUBLE_EQ(translate[1][2],-5);
    ASSERT_DOUBLE_EQ(translate[2][0],0);
    ASSERT_DOUBLE_EQ(translate[2][1],0);
    ASSERT_DOUBLE_EQ(translate[2][2],1);
}

TEST(Matrix33d, testDegToRads)
{
    double deg = 200;
    double rads = Matrix33d::radians(deg);

    ASSERT_NEAR(rads, 3.49, 10e-2);

}

TEST(Matrix33d, testScaleInit)
{
    Matrix33d transform = Matrix33d::scaling(2, -1);

    ASSERT_DOUBLE_EQ(transform[0][0],2);
    ASSERT_DOUBLE_EQ(transform[0][1],0);
    ASSERT_DOUBLE_EQ(transform[0][2],0);
    ASSERT_DOUBLE_EQ(transform[1][0],0);
    ASSERT_DOUBLE_EQ(transform[1][1],-1);
    ASSERT_DOUBLE_EQ(transform[1][2],0);
    ASSERT_DOUBLE_EQ(transform[2][0],0);
    ASSERT_DOUBLE_EQ(transform[2][1],0);
    ASSERT_DOUBLE_EQ(transform[2][2],1);
}

TEST(Matrix33d, testRotateInit)
{
    Matrix33d transform = Matrix33d::rotation(Matrix33d::radians(180));

    ASSERT_NEAR(transform[0][0],-1, 10e-3);
    ASSERT_NEAR(transform[0][1],0, 10e-3);
    ASSERT_NEAR(transform[0][2],0, 10e-3);
    ASSERT_NEAR(transform[1][0],0, 10e-3);
    ASSERT_NEAR(transform[1][1],-1, 10e-3);
    ASSERT_NEAR(transform[1][2],0, 10e-3);
    ASSERT_NEAR(transform[2][0],0, 10e-3);
    ASSERT_NEAR(transform[2][1],0, 10e-3);
    ASSERT_NEAR(transform[2][2],1, 10e-3);
}

TEST(Matrix33d, testRotate45)
{
    Matrix33d transform;
    transform.rotate(Matrix33d::radians(45));

    ASSERT_NEAR(transform[0][0],0.707, 10e-3);
    ASSERT_NEAR(transform[0][1],0.707, 10e-3);
    ASSERT_NEAR(transform[0][2],0, 10e-3);
    ASSERT_NEAR(transform[1][0],-0.707, 10e-3);
    ASSERT_NEAR(transform[1][1],0.707, 10e-3);
    ASSERT_NEAR(transform[1][2],0, 10e-3);
    ASSERT_NEAR(transform[2][0],0, 10e-3);
    ASSERT_NEAR(transform[2][1],0, 10e-3);
    ASSERT_NEAR(transform[2][2],1, 10e-3);
}


TEST(Matrix33d, testMultiply)
{
    Matrix33d a(
            12, -6, 2,
            5, 1, 0,
            0, 0, 1);
    Matrix33d b(
            1, 5, -3,
            -80, 5, 0,
            -6, 8, 7);
    Matrix33d c(
            480, 46, -22,
            -75, 30, -15,
            -6, 8, 7);

    ASSERT_TRUE((a * b)==c);

    a *= b;

    ASSERT_TRUE(a == c);
}

TEST(Matrix33d, testMultiplyVec3f)
{
    Matrix33d transform;
    transform.rotate(Matrix33d::radians(90));

    Vec3f point;
    point[0] = 1;
    point[1] = 1;
    point[2] = 1; // W

    Vec3f pointMoved = transform * point;

    ASSERT_FLOAT_EQ(pointMoved[0], 1);
    ASSERT_FLOAT_EQ(pointMoved[1], -1);
    ASSERT_FLOAT_EQ(pointMoved[2], 1);

}

TEST(Matrix33d, testTransformTranslate)
{
    Vec2f point{-5, 10};
    Matrix33d transform;
    transform.translate(0,2);
    transform.apply(point);

    ASSERT_FLOAT_EQ(point[0], -5);
    ASSERT_FLOAT_EQ(point[1], 12);

    transform = Matrix33d::identity();
    transform.translate(1,-10);
    transform.apply(point);

    ASSERT_FLOAT_EQ(point[0], -4);
    ASSERT_FLOAT_EQ(point[1], 2);

}

TEST(Matrix33d, testApplyWithThirdCoordinate)
{
    Vec3f point{-5, 10, 1}; // W = 10
    Matrix33d transform;
    transform.translate(1,2);
    transform.apply(point);

    ASSERT_FLOAT_EQ(point[0], -4);
    ASSERT_FLOAT_EQ(point[1], 12);

}

TEST(Matrix33d, testApplyIntegrals)
{

    float x = -5.f;
    float y = 10.f;

    Matrix33d transform;
    transform.translate(0,2);
    transform.apply(x, y);

    ASSERT_FLOAT_EQ(x, -5);
    ASSERT_FLOAT_EQ(y, 12);
}

TEST(Matrix33d, testApplyRuntime)
{
    Matrix33d global;
    Matrix33d globalRevert;


    global.translate(1024/2, 1024/2);
    global.scale(1024/2, 1024/2);
    global.invert(globalRevert);

    auto t1 = high_resolution_clock::now();
    for(size_t i = 0; i < 1024; ++i)
    {
        for(size_t j = 0; j < 1024; ++j)
        {
            Vec3f p{float(i), float(j), 1.0};
            //Vec2f p {float(i), float(j)};
            globalRevert.apply(p);
        }
    }
    auto t2 = high_resolution_clock::now();
    auto int_ms = duration_cast<milliseconds>(t2 - t1);

    std::cout << "Total time spent is " << int_ms.count() << " ms\n";

    std::cout << "Average time is " << int_ms.count() / (1024.0 * 1024.0) << " ms\n";

    ASSERT_TRUE(int_ms.count() <= 150.0); // Bellow 100 ms for a 1024*1024 image
}

int main(int argc, char **argv)
{

	testing::InitGoogleTest(&argc, argv);
	setlocale(LC_CTYPE, "");
	return RUN_ALL_TESTS();

	return 0;
}
