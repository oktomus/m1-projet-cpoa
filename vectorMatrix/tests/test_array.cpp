#include <iostream>
#include <stdexcept>

#include "array.h"


#include <gmock/gmock.h>
#include <gtest/gtest.h>

// test sur les entiers
//	EXPECT_EQ, EXPECT_NE, EXPECT_GT, EXPECT_GE, EXPECT_LT, EXPECT_LE, EXPECT_GT

// test sur les réels
//	EXPECT_FLOAT_EQ, EXPECT_DOUBLE_EQ, EXPECT_NEAR

// si on remplace EXPECT par ASSERT le test s'arrete si la condition n'est pas respectee

TEST(ArrayTest, testSize)
{
    Array<int, 10> a1;
    Array<float, 90> a2;
    Array<double, 1> a3;
    Array<char, 1001> a5;

    ASSERT_EQ(a1.length(), 10);
    ASSERT_EQ(a2.length(), 90);
    ASSERT_EQ(a3.length(), 1);
    ASSERT_EQ(a5.length(), 1001);
}


TEST(ArrayTest, testAffect)
{
    Array<int, 5> a1;

    a1[0] = 1;
    a1[1] = 3;
    a1[2] = 30;
    a1[3] = -12;
    a1[4] = 600;

    ASSERT_EQ(a1[0], 1);
    ASSERT_EQ(a1[1], 3);
    ASSERT_EQ(a1[2], 30);
    ASSERT_EQ(a1[3], -12);
    ASSERT_EQ(a1[4], 600);

    a1[0] = -100;

    ASSERT_EQ(a1[0], -100);
    ASSERT_EQ(a1[1], 3);
    ASSERT_EQ(a1[2], 30);
    ASSERT_EQ(a1[3], -12);
    ASSERT_EQ(a1[4], 600);

}

TEST(ArrayTest, testListAffect)
{
    Array<int, 5> a1 = {1, 2, 3, 4, 5};

    ASSERT_EQ(a1[0], 1);
    ASSERT_EQ(a1[1], 2);
    ASSERT_EQ(a1[2], 3);
    ASSERT_EQ(a1[3], 4);
    ASSERT_EQ(a1[4], 5);

}

TEST(ArrayTest, testBadListAffect)
{
    ASSERT_THROW((Array<int, 2>{1, 2, 3, 4, 5}), std::invalid_argument);

}

TEST(ArrayTest, testDefaultValues)
{
    Array<int, 2> a1;
    Array<float, 2> a2;
    Array<double, 2> a3;
    Array<size_t, 2> a4;
    Array<char, 2> a5;

    ASSERT_EQ(a1[0], 0);
    ASSERT_EQ(a1[1], 0);
    ASSERT_FLOAT_EQ(a2[0], .0f);
    ASSERT_FLOAT_EQ(a2[1], .0f);
    ASSERT_DOUBLE_EQ(a3[0], 0.0);
    ASSERT_DOUBLE_EQ(a3[1], 0.0);
    ASSERT_EQ(a4[0], 0);
    ASSERT_EQ(a4[1], 0);
    ASSERT_EQ(a5[0], (char)0);
    ASSERT_EQ(a5[1], (char)0);
}

TEST(ArrayTest, testSwap)
{
    Array<int, 2> a1;
    Array<int, 2> a2;

    a1[0] = 6;
    a1[1] = 12;
    a2[0] = 4;
    a2[1] = -6;

    ASSERT_NO_THROW(a1.swap(a2));

    ASSERT_EQ(a1[0], 4);
    ASSERT_EQ(a1[1], -6);
    ASSERT_EQ(a2[0], 6);
    ASSERT_EQ(a2[1], 12);
}

TEST(ArrayTest, testGetterConst)
{
    Array<int, 1> a1;

    a1[0] = 1;

    int copy1 = a1[0];
    ASSERT_EQ(copy1, 1);
    copy1 = 2;

    ASSERT_EQ(a1[0], 1);

    int &copyRef = a1[0];
    copyRef = 12;

    ASSERT_EQ(a1[0], 12);

}

TEST(ArrayTest, testGetterOutOfRange)
{
    Array<int, 2> a1;

    ASSERT_NO_THROW(a1[0]);
    ASSERT_NO_THROW(a1[1]);
    ASSERT_THROW(a1[-1], std::out_of_range);
    ASSERT_THROW(a1[10], std::out_of_range);
    ASSERT_THROW(a1[-1] = 10, std::out_of_range);
    ASSERT_THROW(a1[10] = 2, std::out_of_range);

}

TEST(ArrayTest, testGetPtr)
{
    Array<int, 2> a1;
    a1[0] = 500;
    a1[1] = 1000;

    int * ptr = a1.get_ptr();

    ASSERT_EQ(*ptr, 500);
    ASSERT_EQ(*(ptr + 1), 1000);

}

TEST(ArrayTest, testEqualityNormal)
{
    Array<int, 2> a1;
    Array<int, 2> a2;

    a1[0] = 100;
    a2[0] = 100;
    a1[1] = 300;
    a2[1] = 300;

    EXPECT_EQ(a1, a2);
    EXPECT_TRUE(a1 == a2);
}

TEST(ArrayTest, testEqualityDifferentSize)
{
    Array<int, 2> a1;
    Array<int, 5> a2;

    a1[0] = 100;
    a2[0] = 100;
    a1[1] = 300;
    a2[1] = 300;

    EXPECT_NE(a1, a2);
    EXPECT_TRUE(a1 != a2);
}

TEST(ArrayTest, testEqualityDifferentType)
{
    Array<int, 2> a1;
    Array<float, 2> a2;

    a1[0] = 100;
    a2[0] = 100.0;
    a1[1] = 300;
    a2[1] = 300.0;

    EXPECT_EQ(a1, a2);
    EXPECT_TRUE(a1 == a2);

    a1[0] = 100;
    a2[0] = 100.05;
    a1[1] = 300;
    a2[1] = 300.01;

    EXPECT_NE(a1, a2);
    EXPECT_FALSE(a1 == a2);
}

TEST(ArrayTest, testEqualityDifferentValues)
{
    Array<int, 2> a1;
    Array<int, 2> a2;

    a1[0] = 100;
    a2[0] = 200;
    a1[1] = 300;
    a2[1] = 300;

    EXPECT_NE(a1, a2);
    EXPECT_FALSE(a1 == a2);
}

TEST(ArrayTest, testFillConstructor)
{
    Array<int, 2> a4;
    a4[0] = 10;
    a4[1] = 20;
    Array<int, 3> a5(a4, 30);

    ASSERT_EQ(a4[0], 10);
    ASSERT_EQ(a4[1], 20);
    ASSERT_EQ(a5[0], 10);
    ASSERT_EQ(a5[1], 20);
    ASSERT_EQ(a5[2], 30);

}

int main(int argc, char **argv)
{
	testing::InitGoogleTest(&argc, argv);
	setlocale(LC_CTYPE, "");
	return RUN_ALL_TESTS();
}
