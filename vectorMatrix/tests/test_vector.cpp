#include <iostream>
#include <stdexcept>

#include "vector.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

TEST(Vector, testEq)
{
    Vector<int, 2> v1;
    Vector<int, 2> v2;
    Vector<int, 2> v3;
    Vector<int, 3> v4;
    v3[0] = 500;

    EXPECT_TRUE(v1==v2);
    EXPECT_TRUE(v3!=v1);
    EXPECT_TRUE(v4!=v1);
}

TEST(Vector, testAddition1D)
{
    Vector<int, 1> v1;
    Vector<int, 1> v2;

    v2[0] = -4;

    v1 += v2;

    ASSERT_EQ(v1[0], -4);
}

TEST(Vector, testAddition2D)
{
    Vector<int, 2> v1;
    Vector<int, 2> v2 {5, -6};

    v1 += v2;

    ASSERT_EQ(v1[0], 5);
    ASSERT_EQ(v1[1], -6);

    v1 += v2;

    ASSERT_EQ(v1[0], 10);
    ASSERT_EQ(v1[1], -12);

    v1 = v1 + v2;

    ASSERT_EQ(v1[0], 15);
    ASSERT_EQ(v1[1], -18);
}

TEST(Vector, testAddition3D)
{
    Vector<int, 3> v1;
    Vector<int, 3> v2 {1, 2, 3};

    v1 += v2;

    ASSERT_EQ(v1[0], 1);
    ASSERT_EQ(v1[1], 2);
    ASSERT_EQ(v1[2], 3);
}

TEST(Vector, testSubstraction1D)
{
    Vector<int, 1> v1;
    Vector<int, 1> v2 {-4};

    v1 -= v2;

    ASSERT_EQ(v1[0], 4);
}

TEST(Vector, testSubstraction2D)
{
    Vector<int, 2> v1;
    Vector<int, 2> v2 {5, -6};

    v1 -= v2;

    ASSERT_EQ(v1[0], -5);
    ASSERT_EQ(v1[1], 6);

    v1 -= v2;

    ASSERT_EQ(v1[0], -10);
    ASSERT_EQ(v1[1], 12);

    v1 = v1 - v2;

    ASSERT_EQ(v1[0], -15);
    ASSERT_EQ(v1[1], 18);

    v1 = v2 - v1;

    ASSERT_EQ(v1[0], 20);
    ASSERT_EQ(v1[1], -24);

}

TEST(Vector, testSubstraction3D)
{
    Vector<int, 3> v1;
    Vector<int, 3> v2 {1, 2, 3};

    v1 -= v2;

    ASSERT_EQ(v1[0], -1);
    ASSERT_EQ(v1[1], -2);
    ASSERT_EQ(v1[2], -3);
}


TEST(Vector, testMultiply1D)
{
    Vector<int, 1> v1;

    v1 *= 0;

    ASSERT_EQ(v1[0], 0);

    v1 = {6};
    v1 = v1 * 10;

    ASSERT_EQ(v1[0], 60);

    v1 = 2 * v1;

    ASSERT_EQ(v1[0], 120);
}

TEST(Vector, testMultiply2D)
{
    Vector<int, 2> v1 {0, 0};

    v1 *= 0;

    ASSERT_EQ(v1[0], 0);
    ASSERT_EQ(v1[1], 0);

    v1 = {6, -2};
    v1 = v1 * 10;

    ASSERT_EQ(v1[0], 60);
    ASSERT_EQ(v1[1], -20);

    v1 = 2 * v1;

    ASSERT_EQ(v1[0], 120);
    ASSERT_EQ(v1[1], -40);
}

TEST(Vector, testMultiply3D)
{
    Vector<int, 3> v1;

    v1 *= 0;

    ASSERT_EQ(v1[0], 0);
    ASSERT_EQ(v1[1], 0);
    ASSERT_EQ(v1[2], 0);

    v1[0] = 6;
    v1[1] = -2;
    v1 = v1 * 10;

    ASSERT_EQ(v1[0], 60);
    ASSERT_EQ(v1[1], -20);
    ASSERT_EQ(v1[2], 0);

    v1 = 2 * v1;

    ASSERT_EQ(v1[0], 120);
    ASSERT_EQ(v1[1], -40);
    ASSERT_EQ(v1[2], 0);
}

TEST(Vector, testDivide1D)
{
    Vector<int, 1> v1;

    v1[0] = 60;
    v1 = v1 / 10;

    ASSERT_EQ(v1[0], 6);

    v1[0] = 60;
    v1 /= 10;

    ASSERT_EQ(v1[0], 6);
}

TEST(Vector, testDivide2D)
{
    Vector<int, 2> v1;

    v1[0] = 6;
    v1[1] = -2;
    v1 = v1 / 2;

    ASSERT_EQ(v1[0], 3);
    ASSERT_EQ(v1[1], -1);
}

TEST(Vector, testDivide3D)
{
    Vector<int, 3> v1;

    v1[0] = 6000;
    v1[1] = -20;
    v1 = v1 / 4;

    ASSERT_EQ(v1[0], 1500);
    ASSERT_EQ(v1[1], -5);
    ASSERT_EQ(v1[2], 0);
}

TEST(Vector, testDivideByZero)
{
    Vector<int, 1> v1;

    ASSERT_THROW(v1 / 0, std::overflow_error);

    Vector<float, 1> v3;

    ASSERT_NO_THROW(v3/(0.004f));

}

TEST(Vector, testDot1D)
{
    Vector<int, 1> v1;
    Vector<int, 1> v2;

    v1[0] = 5;
    v2[0] = 2;

    ASSERT_EQ(v1*v2, 10);
}

TEST(Vector, testDot2D)
{
    Vector<int, 2> v1;
    Vector<int, 2> v2;

    v1[0] = 1;
    v1[1] = 0;
    v2[0] = 5;
    v2[1] = -6;

    ASSERT_EQ(v1*v2, 5);
}

TEST(Vector, testDotFloat)
{
    Vector<float, 2> v1;
    Vector<float, 2> v2;

    v1[0] = 1.0;
    v1[1] = 0.005;
    v2[0] = 5;
    v2[1] = -10;

    ASSERT_FLOAT_EQ(v1*v2, 4.95);
}

TEST(Vector, testCross3D)
{
    Vector<float, 3> v1;
    Vector<float, 3> v2;

    v1[0] = 2;
    v1[1] = 3;
    v1[2] = 4;

    v2[0] = 5;
    v2[1] = 6;
    v2[2] = 7;

    Vector<float, 3> res = v1^v2;

    ASSERT_FLOAT_EQ(res[0], -3);
    ASSERT_FLOAT_EQ(res[1], 6);
    ASSERT_FLOAT_EQ(res[2], -3);
}

TEST(Vector, testCross4D)
{
    Vector<float, 4> v1;
    Vector<float, 4> v2;

    ASSERT_THROW(v1^v2, std::invalid_argument);

}

TEST(Vector, testVec2Init)
{
    Vec2f a1{12, -6};

    ASSERT_EQ(a1[0], 12);
    ASSERT_EQ(a1[1], -6);
}

TEST(Vector, testVec3Init)
{
    Vec3f a1{12, -6, 100};

    ASSERT_EQ(a1[0], 12);
    ASSERT_EQ(a1[1], -6);
    ASSERT_EQ(a1[2], 100);

    Vec3i a2{12, -6, 200};

    ASSERT_EQ(a2[0], 12);
    ASSERT_EQ(a2[1], -6);
    ASSERT_EQ(a2[2], 200);
}

TEST(Vector, testVec4Init)
{
    Vec4f a1{12, -6, 500, .018};

    ASSERT_FLOAT_EQ(a1[0], 12);
    ASSERT_FLOAT_EQ(a1[1], -6);
    ASSERT_FLOAT_EQ(a1[2], 500);
    ASSERT_FLOAT_EQ(a1[3], .018);
}

TEST(Vector, testFillConstructor)
{
    Vec2i a4{10, 20};
    Vec3i a5{a4, 30};

    ASSERT_EQ(a4[0], 10);
    ASSERT_EQ(a4[1], 20);
    ASSERT_EQ(a5[0], 10);
    ASSERT_EQ(a5[1], 20);
    ASSERT_EQ(a5[2], 30);

}

TEST(Vector, testUnsignedChar)
{
    ASSERT_NO_THROW((Vec3uc {255, 255, 30}));
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    setlocale(LC_CTYPE, "");
    return RUN_ALL_TESTS();
}
