#ifndef __MATRIX33D__
#define __MATRIX33D__

#include <cstddef>
#include <iostream>
#include <cmath>
#include "vector.h"

#define PI          3.14159265359 /* pi */
#define PIby180     0.01745329252 /* pi / 180 */

/*
                                                               
                 ▄             ▀            ▄▄▄▄   ▄▄▄▄      █ 
 ▄▄▄▄▄   ▄▄▄   ▄▄█▄▄   ▄ ▄▄  ▄▄▄    ▄   ▄  ▀   ▀█ ▀   ▀█  ▄▄▄█ 
 █ █ █  ▀   █    █     █▀  ▀   █     █▄█     ▄▄▄▀   ▄▄▄▀ █▀ ▀█ 
 █ █ █  ▄▀▀▀█    █     █       █     ▄█▄       ▀█     ▀█ █   █ 
 █ █ █  ▀▄▄▀█    ▀▄▄   █     ▄▄█▄▄  ▄▀ ▀▄  ▀▄▄▄█▀ ▀▄▄▄█▀ ▀█▄██ 
                                                               
 */

//////////////////////////////////////
/// Definition
//////////////////////////////////////

/**
  * Representation of a matrix for 2D transformation
  */
class Matrix33d
{

public:

    /**
      * Default constructor, identoty matrix
      */
    Matrix33d();

    /**
      * Constructor with all values 
      *
      */
    Matrix33d(
            const double r1c1,
            const double r1c2,
            const double r1c3,
            const double r2c1,
            const double r2c2,
            const double r2c3,
            const double r3c1,
            const double r3c2,
            const double r3c3
            );

    /**
      * Build a 3x3 identity matrix
      *
      * @return A matrix
      */
    static Matrix33d identity()
    {
        return Matrix33d();
    }

    /**
      * Create a 2D transformation matrix for transalting
      *
      * @param x    Amount of x translation
      * @param y    Amount of y translation
      * @return     A matrix in homogeneous coodrinate
      */
    static Matrix33d translation(const double& x, const double& y)
    {
        return Matrix33d(1, 0, x, 0, 1, y, 0, 0, 1);
    }

    /**
      * Apply a translation on the current matrix
      *
      * @param x    X translation
      * @param y    Y translation
      */
    void translate(const double &x, const double &y)
    {
        (*this) *= Matrix33d::translation(x, y);
    }

    /**
      * Convert a given degrea value to radians
      *
      * @param deg  Degreas
      * @return     The randians as a double
      */
    static double radians(const double deg)
    {
        return deg * PIby180;
    }

    /**
      * Create a rotation matrix
      *
      * @param rads     Rotation amount in radians
      * @return     A matrix in homogeneous coordinate
      */
    static Matrix33d rotation(const double& rads)
    {
        return Matrix33d(
                cos(rads), sin(rads), 0,
                -sin(rads), cos(rads), 0,
                0, 0, 1);
    }

    /**
      * Apply a rotation matrix on the current matrix
      *
      * @param rads     Rotation amount
      */
    void rotate(const double &rads)
    {
        (*this) *= Matrix33d::rotation(rads);
    }

    /**
      * Create a scale matrix
      *
      * @param scale_x      Scale amount in X
      * @param scale_y      Scale amount in Y
      * @return     A matrix in homogeneous coordinate
      */
    static Matrix33d scaling(const double &scale_x, const double &scale_y)
    {
        return Matrix33d(
                scale_x, 0, 0,
                0, scale_y, 0,
                0, 0, 1);
    }

    /**
      * Apply a scale on the current matrix
      *
      * @param scale_x      Scale amount in X
      * @param scale_y      Scale amount in Y
      */
    void scale(const double & scale_x, const double & scale_y)
    {
        (*this) *= Matrix33d::scaling(scale_x, scale_y);
    }


    /**
      * Find the inverse of the matrix if there is any
      * 
      * The result is output in the arg out
      * If no inverse is available, a code different
      * from Zero is returned
      *
      * @param out  The output matrix
      * @return int An error or success code
      */
    int invert (Matrix33d& out);

    /**
      * Apply the matrix onto a 2D vector
      *
      * @param point    The vector point
      * @return         The transformed point
      */
    template<typename T>
    void apply(Vector<T, 2>& point) const
    {
        Vector<T, 2> copy(point);

        point[0] =
            data[0][0] * copy[0] +
            data[0][1] * copy[1] +
            data[0][2];

        point[1] =
            data[1][0] * copy[0] +
            data[1][1] * copy[1] +
            data[1][2];
    }

    /**
      * Apply the matrix onto a 2D vector and apply the W coordinate
      *
      * @param point    The vector point
      * @return         The transformed point
      */
    template<typename T>
    void apply(Vector<T, 3>& point) const
    {
        point = (*this) * point;
    }

    template<typename T>
    void apply(T& x, T& y) const
    {
        Vector<T, 3> homogeneous{x, y, 1};
        apply(homogeneous);
        x = homogeneous[0];
        y = homogeneous[1];
    }

    void writeTo(std::ostream& out) const
    {
        out << data[0][0] << " " << data[0][1] << " " << data[0][2] << "\n" <<
                data[1][0] << " " << data[1][1] << " " << data[1][2] << "\n" <<
                data[2][0] << " " << data[2][1] << " " << data[2][2];
    }

    /**
      * Matrix accessor
      *
      * @param row      The row to access
      * @return         A pointer to the wanted row
      */
    double* operator[] (const size_t row)
    {
        return data[row];
    }
    
    /**
      * Matrix const accessor. Read-only
      *
      * @param row      The row to access
      * @return         A const pointer to the wanted row
      */
    const double* operator[] (const size_t row) const
    {
        return data[row];
    }

    /**
      * Check if two matrix are equals or not
      *
      * @param a    Matrix A
      * @param b    Matrix B
      * @return     A boolean
      */
    friend bool operator==(const Matrix33d& a, const Matrix33d& b);

    /**
      * The opposite of operator ==
      *
      * @param a    Matrix A
      * @param b    Matrix B
      * @return     A boolean
      */
    friend bool operator!=(const Matrix33d& a, const Matrix33d& b);

    /**
      * Convert the matrix into an output stream
      */
    friend std::ostream& operator<< (std::ostream& a, const Matrix33d& b);

    friend std::istream &operator>>( std::istream  &input, Matrix33d &m ) {
        input >> m[0][0];
        input >> m[0][1];
        input >> m[0][2];
        input >> m[1][0];
        input >> m[1][1];
        input >> m[1][2];
        input >> m[2][0];
        input >> m[2][1];
        input >> m[2][2];
        return input;
    }

    /**
      * Multiplication between matrices
      */
    Matrix33d& operator*= (const Matrix33d& b);
    friend Matrix33d operator* (Matrix33d a, const Matrix33d& b);

    /**
      * Multiplication with a vector
      */
    template<typename T>
    friend Vector<T, 3> operator* (const Matrix33d& a, Vector<T, 3> v)
    {
        Vector<T, 3> copy(v);

        v[0] = 
            T(a[0][0]) * copy[0] +
            T(a[0][1]) * copy[1] +
            T(a[0][2]) * copy[2];

        v[1] = 
            T(a[1][0]) * copy[0] +
            T(a[1][1]) * copy[1] +
            T(a[1][2]) * copy[2];

        v[2] = 
            T(a[2][0]) * copy[0] +
            T(a[2][1]) * copy[1] +
            T(a[2][2]) * copy[2];

        return v;
    }

protected:

    /**
      * Matrix values
      * First indice is the row, second is the column
      */
    double data[3][3] = {{0}};
    
};

#endif
