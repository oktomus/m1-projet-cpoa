#include "matrix33d.h"

/*
                                                               
                 ▄             ▀            ▄▄▄▄   ▄▄▄▄      █ 
 ▄▄▄▄▄   ▄▄▄   ▄▄█▄▄   ▄ ▄▄  ▄▄▄    ▄   ▄  ▀   ▀█ ▀   ▀█  ▄▄▄█ 
 █ █ █  ▀   █    █     █▀  ▀   █     █▄█     ▄▄▄▀   ▄▄▄▀ █▀ ▀█ 
 █ █ █  ▄▀▀▀█    █     █       █     ▄█▄       ▀█     ▀█ █   █ 
 █ █ █  ▀▄▄▀█    ▀▄▄   █     ▄▄█▄▄  ▄▀ ▀▄  ▀▄▄▄█▀ ▀▄▄▄█▀ ▀█▄██ 
                                                               
 */

Matrix33d::Matrix33d() : Matrix33d(1, 0, 0, 0, 1, 0, 0, 0, 1)
{
}

Matrix33d::Matrix33d(
        const double r1c1,
        const double r1c2,
        const double r1c3,
        const double r2c1,
        const double r2c2,
        const double r2c3,
        const double r3c1,
        const double r3c2,
        const double r3c3
        )
{
    data[0][0] = r1c1;
    data[0][1] = r1c2;
    data[0][2] = r1c3;
    data[1][0] = r2c1;
    data[1][1] = r2c2;
    data[1][2] = r2c3;
    data[2][0] = r3c1;
    data[2][1] = r3c2;
    data[2][2] = r3c3;
}

int Matrix33d::invert(Matrix33d& out)
{
    // Algorithm: Gauss-Jordan Elimination
    // Linear row reduction

    // Out will be the extension of the original matrix
    out = identity();
    Matrix33d edit((*this));

    // Basic row operations used in this algo

    // Revert 2 rows
    auto exchangeRows = [&] (size_t r1, size_t r2) -> void
    {
        double tmp;
        for(size_t j = 0; j < 3; j++)
        {
            // Main row
            tmp = edit[r1][j];
            edit[r1][j] = edit[r2][j];
            edit[r2][j] = tmp;
            // Extension row
            tmp = out[r1][j];
            out[r1][j] = out[r2][j];
            out[r2][j] = tmp;
        }
    };

    auto multiplyRow = [&] (size_t r, double mult) -> void
    {
        for(size_t j = 0; j < 3; j++)
        {
            // Main row
            edit[r][j] *= mult;
            // Extension row
            out[r][j] *= mult;
        }
    };

    auto addTimesRow = [&] (
            size_t row_from, 
            size_t row_to,
            double mult) -> void
    {
        for(size_t j = 0; j < 3; j++)
        {
            // Main row
            edit[row_to][j] += (mult * edit[row_from][j]);
            // Extension row
            out[row_to][j] += (mult * out[row_from][j]);
        }
    };



    // [row][column] [i][j]

    // From column 1 to n
    for(size_t j = 0; j < 3; ++j)
    {
        // Find row i with i >= j and Mij is the largest absolute double
        // If any, then can't be inverted

        double max = -1.0, val;
        size_t max_row;
        size_t i;
        for( i = j; i < 3; i++)
        {
            val = fabs(edit[i][j]);
            if (val != 0 && val > max)
            {
                max = val;
                max_row = i;
            }
        }
        if (max == -1.0)
        {
            // FAIL !
            return 2;
        }

        // Row found
        // Exchange row if max_row != j
        if (max_row != j)
        {
            exchangeRows(max_row, j);
        }

        // Multiply row j by 1/Mjj
        multiplyRow(j, 1/edit[j][j]);

        // For each row 1 <= r <= n
        for(size_t r = 0; r < 3; ++r)
        {
            if(r != j)
            {
                // Add -Mrj time row j to row r
                addTimesRow(j, r, -edit[r][j]);
            }
        }
    }

    return 0;
}


bool operator==(const Matrix33d& a, const Matrix33d& b)
{
    for(size_t j = 0; j < 3; ++j)
    {
        for(size_t r = 0; r < 3; ++r)
        {
            if(a[j][r] != b[j][r])
                return false;
        }
    }
    return true;
}

bool operator!=(const Matrix33d& a, const Matrix33d& b)
{
    return !(a == b);
}

std::ostream& operator<< (std::ostream& a, const Matrix33d& b)
{
    for (size_t row = 0; row < 3; ++row)
    {
        a << "[" << 
            b.data[row][0] << ", " << 
            b.data[row][1] << ", " << 
            b.data[row][2] << "]\n"; 
    }

    return a;

}

Matrix33d& Matrix33d::operator*= (const Matrix33d& b)
{
    Matrix33d copy((*this));

    (*this)[0][0] = 
        copy[0][0] * b[0][0] +
        copy[0][1] * b[1][0] +
        copy[0][2] * b[2][0];
    (*this)[0][1] = 
        copy[0][0] * b[0][1] +
        copy[0][1] * b[1][1] +
        copy[0][2] * b[2][1];
    (*this)[0][2] = 
        copy[0][0] * b[0][2] +
        copy[0][1] * b[1][2] +
        copy[0][2] * b[2][2];

    (*this)[1][0] = 
        copy[1][0] * b[0][0] +
        copy[1][1] * b[1][0] +
        copy[1][2] * b[2][0];
    (*this)[1][1] = 
        copy[1][0] * b[0][1] +
        copy[1][1] * b[1][1] +
        copy[1][2] * b[2][1];
    (*this)[1][2] = 
        copy[1][0] * b[0][2] +
        copy[1][1] * b[1][2] +
        copy[1][2] * b[2][2];

    (*this)[2][0] = 
        copy[2][0] * b[0][0] +
        copy[2][1] * b[1][0] +
        copy[2][2] * b[2][0];
    (*this)[2][1] = 
        copy[2][0] * b[0][1] +
        copy[2][1] * b[1][1] +
        copy[2][2] * b[2][1];
    (*this)[2][2] = 
        copy[2][0] * b[0][2] +
        copy[2][1] * b[1][2] +
        copy[2][2] * b[2][2];

    return *this;
}

Matrix33d operator* (Matrix33d a, const Matrix33d& b)
{
    a *= b;
    return a;
}

