#ifndef __ARRAY__
#define __ARRAY__

#include <iostream>
#include <string>
#include <sstream>
#include <stdexcept>
#include <initializer_list>
#include <algorithm>

/*
                                           █     
  ▄▄▄    ▄ ▄▄   ▄ ▄▄   ▄▄▄   ▄   ▄         █ ▄▄  
 ▀   █   █▀  ▀  █▀  ▀ ▀   █  ▀▄ ▄▀         █▀  █ 
 ▄▀▀▀█   █      █     ▄▀▀▀█   █▄█          █   █ 
 ▀▄▄▀█   █      █     ▀▄▄▀█   ▀█      █    █   █ 
                              ▄▀                 
                             ▀▀                  
*/

//////////////////////////////////////
/// Definition
//////////////////////////////////////

/**
  * Create an array of given size and given type.
  * Specify the values type first, then the size. Must be positive.
  */
template <typename T, size_t S>
class Array
{
public:

    /**
      * Default constructor. Does nothing special.
      */
    Array();

    /**
      * Copy constructor. Create an array with another values.
      *
      * @param other    The array from which the new array
      *     will be created
      */
    Array(const Array &other);

    /**
      * Create an array by gicing all values at once
      *
      * @param      The values
      */
    Array(std::initializer_list<T>);

    /**
      * Create an array with an array of size - 1
      *
      * Usefull for vectors (vec3(vec2, n))
      *
      * @param a    The other array of lower size
      * @param fill The value to use to fill the array
      */
    Array(const Array<T, S-1> &a, T fill);
        
    /**
      * Return the length of the array.
      *
      * @return     The array length as an unisgned int
      */
    size_t length() const;

    /**
      * Swap the values between two arrays.
      *
      * @param b    The other array
      */
    void swap(Array &b);

    /**
      * Returns a ptr to the values array
      */
    T* get_ptr();

    /**
      * Access a value of the array to modify it.
      *
      * Fails if the given index is < 0 or >= length
      *
      * @param id   The index of the value
      * @return     A reference to the requested value
      */
    T& operator[] (const size_t id);

    /**
      * Access a value of the array as const. Use in const members.
      *
      * Fails if the given index is < 0 or >= length
      *
      * @param id   The index of the value
      * @return     A copy of the requested value
      */
    const T& operator[] (const size_t id) const;

    /**
      * Test the equality between two arrays.
      *
      * Two arrays are equals if:
      * - they are the same size and;
      * - each value at each index is the same in each array.
      *
      * @param a    Array a
      * @param b    Array b
      * @return     True or False
      */
    template <typename T1, size_t S1, typename T2, size_t S2>
    friend bool operator== (const Array<T1, S1> &a, const Array<T2, S2> &b);
    
    /**
      * Test the inequality between two arrays.
      *
      * The reverse of the equality
      *
      * @param a    Array a
      * @param b    Array b
      * @return     True or False
      */
    template <typename T1, size_t S1, typename T2, size_t S2>
    friend bool operator!= (const Array<T1, S1> &a, const Array<T2, S2> &b);

    /**
      * Assign operator.
      * 
      * Will copy other values into a new array
      *
      * @param other    The other array
      * @return         The new Array
      */
    Array& operator= (const Array &other) = default;

    /**
      * Convert the array into an output stream.
      *
      * @param out      The output stream
      * @param o        The array to stream
      * @return         The modified stream
      */
    template <typename T1, size_t S1>
    friend std::ostream& operator<<(std::ostream& out, const Array<T1, S1> &o);

private:

    /**
      * Check if the given index is correct for the array
      *
      * Will throw an exception if index is out of range
      * Method used for the operator []
      *
      * @param      The index of the value
      * @return     True if it is
     */
    bool check_index(const size_t) const;

protected:

    /**
      * The array values
      */
    T values[S];

    /**
      * The size of the array.
      *
      * We could use the template S in any method but I found it more
      * clear to access the length via a method.
      */
    size_t size;

};

//////////////////////////////////////
/// Implementation
//////////////////////////////////////

/**
  * Default constructor.
  *
  * Initilialize the size.
  * Valuez are automatically initialized since they are not
  * dynamic
  */
template <typename T, size_t S>
Array<T, S>::Array():
    values{0},
    size(S)
{
}

template <typename T, size_t S>
Array<T, S>::Array(std::initializer_list<T> parray):
    values{},
    size(S)
{
    if(parray.size() != S)
    {
        throw std::invalid_argument("Size of argument and array don't match.");
    }
    std::copy(parray.begin(), parray.end(), values);
}

/**
  * Create an array with other's values
  */
template <typename T, size_t S>
Array<T, S>::Array(const Array<T, S> &other) : 
    values{0},
    size(S)
{
    size_t i;
    for(i = 0; i < S; ++i)
    {
        values[i] = other[i];
    }
}

template <typename T, size_t S>
Array<T, S>::Array(const Array<T, S-1> &a, const T fill) : 
    values{},
    size(S)
{
    size_t i;
    for(i = 0; i < S-1; ++i)
    {
        values[i] = a[i];
    }
    values[i] = fill;
}

/**
  * Returns the array length
  */
template <typename T, size_t S>
size_t Array<T, S>::length() const
{
    return size;
}

template <typename T, size_t S>
T* Array<T, S>::get_ptr()
{
    return values;
}

/**
  * Swap the values from an array to another.
  */
template <typename T, size_t S>
void Array<T, S>::swap(Array<T, S> &b)
{
    size_t i = 0;

    // Swap array values
    T temp;
    for(i = 0; i < length(); ++i)
    {
        temp = values[i];
        values[i] = b.values[i];
        b.values[i] = temp;     
    }
}

/**
  * Access a value in the array
  *
  * Will fail for an index out of bound
  */
template <typename T, size_t S>
T& Array<T, S>::operator[] (const size_t id)
{
    check_index(id);
    return values[id];
}   

/**
  * Access a value in the array
  *
  * Will fail for an index out of bound
  */
template <typename T, size_t S>
const T& Array<T, S>::operator[] (const size_t id) const
{
    check_index(id);
    return values[id];
}   

/**
  * Returns the given stream with each value of the array
  *
  * Array values have to be printable
  */
template <typename T1, size_t S1>
std::ostream& operator<<(std::ostream& out, const Array<T1, S1> &o)
{
    out << "[";
    size_t i = 0;
    for(i = 0; i < (o.length() - 1); ++i)
    {
        out << o.values[i];
        out << ", ";
    }
    // i est quand meme incremnter a la fin de la boucle
    out << o.values[i];
    out << "]";
    return out;
}

/**
  * Test the equality between two arrays
  */
template <typename T1, size_t S1, typename T2, size_t S2>
bool operator== (const Array<T1, S1> &a, const Array<T2, S2> &b)
{
    if (a.length() != b.length())
    {
        return false;
    }

    // Go through each value
    size_t i = 0;
    while(i < a.length())
    {
        if (a[i] != b[i])
        {
            return false;
        }
        ++i;
    }
    return true;
}   

template <typename T1, size_t S1, typename T2, size_t S2>
bool operator!= (const Array<T1, S1> &a, const Array<T2, S2> &b)
{
    return !(a == b);
}   

/**
  * Check bounds
  * If array is of size 0, an exception will still be thrown
  */
template <typename T, size_t S>
bool Array<T, S>::check_index(const size_t id) const
{
    if(id < 0 || id >= size)
    {
        std::stringstream ss;
        ss << "Index " << id << " is out of range";
        throw std::out_of_range(ss.str());
    }
    return true;
}   

#endif
