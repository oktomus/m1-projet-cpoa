#!/bin/bash
set -x

git submodule update --init googletest

# nettoie le repertoire de build
rm -rf build_gt GT
mkdir build_gt/ GT/
cd build_gt

export MAKEFLAGS=-j`nproc` 

# compile et install 

# en Debug
cmake ../googletest/ -DCMAKE_BUILD_TYPE=Debug -DBUILD_GTEST=true -DCMAKE_INSTALL_PREFIX=../GT
cmake --build . --target install
# en Release
cmake . -DCMAKE_BUILD_TYPE=Release -DBUILD_GTEST=true -DCMAKE_INSTALL_PREFIX=../GT
cmake --build . --target install

rm -rf build_gt
