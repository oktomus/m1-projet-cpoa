#include "renderImg.h"
#include <QMouseEvent>
#include <iostream>

//#include "imgGradient.h"
#include "boundingBox.h"
#include <limits>



RenderImg::RenderImg(BoundingBox& bb, QWidget *parent ):
	QGLWidget(QGLFormat(QGL::SampleBuffers), parent),
	m_texture(0),
	m_widthTex(0),
	m_heightTex(0),
	m_ptrTex(NULL),
	m_img(1024, 1024),
	m_drawSobel(false),
    m_BBdraw(false),
    m_BB(bb)
  // QQ INIT A AJOUTER ?

{
    //  A ajouter au debut du constructeur de RenderImg
    m_timer = new QTimer(this);
    m_timer->setInterval(20);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(animate()));

    // Creation des particules
    static float nb_particles = 1000;
    for(size_t i = 0; i < nb_particles; ++i)
    {
        m_particles.push(Particle(i, 0.0, 0.0));
    }

    m_ptrTex = m_img.get_pixels();
    m_widthTex = m_img.width;
    m_heightTex = m_img.height;

}


void RenderImg::loadTexture(const std::string& filename)
{
	// VOTRE CODE ICI
    m_drawSobel = false;
        Image2Grey fichier(filename);
        m_img.swap(fichier);

        updateDataTexture();
}



void RenderImg::updateDataTexture()
{
        m_widthTex = m_img.width;
        m_heightTex = m_img.height;
	m_ptrTex = m_img.get_pixels();
	glBindTexture(GL_TEXTURE_2D, m_texture);
	glTexSubImage2D(GL_TEXTURE_2D,0,0,0,m_widthTex, m_heightTex, GL_LUMINANCE, GL_UNSIGNED_BYTE, m_ptrTex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, m_widthTex, m_heightTex, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, m_ptrTex);
	glBindTexture(GL_TEXTURE_2D, 0);
}

Image2Grey & RenderImg::getImg()
{
    return m_img;
}

unsigned int RenderImg::getWidth()
{
    return m_img.width;
}

unsigned int RenderImg::getHeight()
{
    return m_img.height;
}

RenderImg::~RenderImg()
{
	glDeleteTextures(1, &m_texture);
	m_texture = 0;
}

void RenderImg::initializeGL()
{
	glClearColor(0.0f,0.0f,0.4f,0.0f);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);

	glGenTextures(1, &m_texture);
	glBindTexture(GL_TEXTURE_2D, m_texture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, m_widthTex, m_heightTex, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, m_ptrTex);
	glBindTexture(GL_TEXTURE_2D, 0);

	update();


}

void RenderImg::paintGL()
{
	glClear(GL_COLOR_BUFFER_BIT);
        glLoadIdentity();

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, m_texture);

	glColor3f(1.0,1.0,1.0);
	glBegin(GL_QUADS);
	glTexCoord2f(0,0);
	glVertex2f( -1,1);
	glTexCoord2f(0,1);
	glVertex2f(-1,-1);
	glTexCoord2f(1,1);
	glVertex2f( 1,-1);
	glTexCoord2f(1,0);
	glVertex2f( 1,1);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);

        // Points
        glPointSize(5.0);
        glColor3f(1, 0, 0);
        glBegin(GL_POINTS);
        glVertex2f(0, 0);
        /*for(int i = 0; i < 50; i++)
        {
            glVertex2f(points[i][0], points[i][1]);
        }*/
        glEnd();

	// for debugging
	if (m_drawSobel)
		drawSobel();

        //animate();

	if (m_BBdraw)
		drawBB(m_BB);
	swapBuffers();

}

void RenderImg::resizeGL(int width, int height)
{
	m_winW = width;
	m_winH = height;
	int side = qMin(width, height);
	m_winS = side;
	glViewport((width - side) / 2, (height - side) / 2, side, side);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	update();
}

void RenderImg::coordInTexture(QMouseEvent *event, int& x, int& y)
{
	if (m_winS == m_winW)
	{
		x = (float(event->x())/m_winW)*m_widthTex;
		y = (float(event->y()-(m_winH-m_winS)/2)/m_winH)*m_heightTex;
	}
	else
	{
		x = (float(event->x()-(m_winW-m_winS)/2)/m_winH)*m_widthTex;
		y = (float(event->y())/m_winH)*m_heightTex;
	}
}


void RenderImg::mousePressEvent(QMouseEvent *event)
{
	int x,y;
	coordInTexture(event, x, y);
	m_lastPos.setX(x);
	m_lastPos.setY(y);

	std::cout << "Mouse-press in widget"<< event->x() << " / "<< event->y() << std::endl;
	std::cout << " =>  position in texture "<< x << " / "<< y << std::endl;

	if (m_state_modifier & Qt::ShiftModifier)
		std::cout << "     with Shift" << std::endl;
	if (m_state_modifier & Qt::ControlModifier)
		std::cout << "     with Ctrl" << std::endl;

        update();

        while(m_particles.size() > 0 && m_particles.top().getTime() < globalTime)
        {
            Particle a = m_particles.top();
            m_particles.pop();
            a.move();
            glVertex2f(2.0f*a.pos_[0]/m_widthTex-1.0f, -2.0f*a.pos_[1]/m_heightTex+1.0f);
            m_particles.push(a);
        }

        /*

	paintGL();

	glPointSize(4.0f);
	glColor3f(1.0f,0,0);
	glBegin(GL_POINTS);

	unsigned int nbp = 0;// VOTRE CODE ICI : nombre de particules
	for (int i = 0; i < nbp; i++ )
	{
		// here get back position of each particle in ptPos
//		glVertex2f(2.0f*ptPos[0]/m_widthTex-1.0f, -2.0f*ptPos[1]/m_heightTex+1.0f);
	}
	glEnd();
        */

}


void RenderImg::mouseReleaseEvent(QMouseEvent *event)
{
//	int x,y;
//	coordInTexture(event, x, y);
//	m_lastPos.setX(x);
//	m_lastPos.setY(y);

//	std::cout << " RELEASE in texture "<< x << " / "<< y << std::endl;

}

void RenderImg::keyPressEvent(QKeyEvent* event)
{
	m_state_modifier = event->modifiers();

	switch(event->key())
	{
		case 'A':
			std::cout << " touche a enfoncee" << std::endl;
			break;
		case 'E':
			// qq init
			m_timer->start();
			break;
		case 'S':
			m_timer->stop();
			// ??
			break;
		case 'P':
			if (m_timer->isActive())
				m_timer->stop();
			else
				m_timer->start();
			break;
	}

	update();

}


void RenderImg::animate()
{
    /*
        for(int i = 0; i < 50; i++)
        {
            points[i].avance();
        }
        */
	update();
}



void RenderImg::keyReleaseEvent(QKeyEvent* /*event*/)
{
	m_state_modifier = 0;
}


void RenderImg::clean()
{
	unsigned char* ptr=m_ptrTex;

	for (int i=0; i<m_heightTex; ++i)
	{
		for (int j=0; j<m_widthTex; ++j)
		{
			*ptr++ = 0;
		}
	}
}

void RenderImg::toggleSobel()
{
	m_drawSobel = !m_drawSobel;
	if (m_drawSobel)
	{
            Image2Grey smooth = Image2Grey::smooth(m_img, 2);
            //m_img = Image2Grey::smooth(m_img, 2);
            m_img.swap(smooth);
            m_grad.reset();
            m_grad = std::unique_ptr<GradientSobel>(
                        new GradientSobel(m_img));
            m_img.seuil(128);
            updateDataTexture();
	}
        else
        {
            update();
        }
}

void RenderImg::smoothImg()
{
    Image2Grey smooth = Image2Grey::smooth(m_img, 2);
    m_img.swap(smooth);
    updateDataTexture();
}

void RenderImg::downscaleImg()
{
    Image2Grey small = Image2Grey::downscale(m_img);
    m_img.swap(small);
    updateDataTexture();
}

void RenderImg::thresholdImg()
{
    m_img.seuil(128);
    updateDataTexture();
}


void RenderImg::drawSobel()
{
	glBegin(GL_LINES);
	for (int j=0; j<m_heightTex; j+=2)
	{
		for (int i=0; i<m_widthTex; i+=2)
		{
			// get value of gradiant
            Vec2f v = (*m_grad)(i, j);

			if (v*v > 0.001f)
			{
				float x = -1.0f + (2.0f*i)/(m_widthTex-1);
				float y = 1.0f  - (2.0f*j)/(m_heightTex-1);// minus because of GL is bottom to up and image up to boytom
				v *= 4.0f/m_widthTex;
				glColor3f(1.0f,1.0f,0.0f);
				glVertex2f(x,y);
				glColor3f(1.0f,0.0f,0.0f);
				glVertex2f(x+v[0]/2,y-v[1]/2);
			}
		}
	}
	glEnd();
}


void RenderImg::drawBB(const BoundingBox& bb)
{
	glBegin(GL_LINE_LOOP);
	glColor3f(1.0f,0.5f,0.5f);
        /*
	glVertex2f( xImg2GL(??), yImg2GL(??) );
	glVertex2f( xImg2GL(??), yImg2GL(??) );
	glVertex2f( xImg2GL(??), yImg2GL(??) );
	glVertex2f( xImg2GL(??), yImg2GL(??) );
        */
        glVertex2f(bb.xmin(),  - bb.ymin());
        glVertex2f(bb.xmax(),  - bb.ymin());
        glVertex2f(bb.xmax(),  - bb.ymax());
        glVertex2f(bb.xmin(),  - bb.ymax());
	glEnd();
}

