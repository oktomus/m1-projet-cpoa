#include "mainwindow.h"
#include "ui_mainwindow.h"

#include"csgPrimitive.h"
#include"csgDisk.h"
#include"csgRegularPolygon.h"

#include <QFileDialog>
#include <QTextCursor>
#include <QSpinBox>

#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>
#include <chrono>
#include <cassert>
#include <exception>

using namespace std::chrono;


MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow),
    m_currentNode(NULL),
    m_prim(NULL),
    m_oper(NULL),
	m_graphTextEdit(NULL),
	m_stopSignal(false),
        m_bb({0, 0}, {.5f, .3f})

{
	ui->setupUi(this);

    m_render = new RenderImg(m_bb);

	ui->HLayout->insertWidget(0,m_render,99);
	m_render->setFocusPolicy(Qt::ClickFocus);

	ui->translationX->setMinimum(-100);
	ui->translationX->setMaximum(100);
	ui->translationY->setMinimum(-100);
	ui->translationY->setMaximum(100);
	
	ui->scale->setMinimum(-100);
	ui->scale->setMaximum(100);
	
    ui->currentNode->setMinimum(0);
    ui->currentNode->setValue(0);
    ui->currentNode->setEnabled(false);
    ui->id_filsDroit->setValue(0);
    ui->id_filsGauche->setValue(0);

    updateMaximumIds();
	
	connect(ui->create_oper,SIGNAL(clicked()),SLOT(createOperation()));
	connect(ui->create_prim,SIGNAL(clicked()),SLOT(createPrimtive()));
	connect(ui->resetTransfo,SIGNAL(clicked()),SLOT(resetTransfo()));
	connect(ui->applyTransfo,SIGNAL(clicked()),SLOT(applyTransfo()));

	connect(ui->translationX,SIGNAL(valueChanged(int)),SLOT(transfoSliderChanged()));
	connect(ui->translationY,SIGNAL(valueChanged(int)),SLOT(transfoSliderChanged()));
	connect(ui->rotation,SIGNAL(valueChanged(int)),SLOT(transfoSliderChanged()));
	connect(ui->scale,SIGNAL(valueChanged(int)),SLOT(transfoSliderChanged()));

	connect(ui->dsb_tx,SIGNAL(valueChanged(double)),SLOT(transfoSpinChanged()));
	connect(ui->dsb_ty,SIGNAL(valueChanged(double)),SLOT(transfoSpinChanged()));
	connect(ui->dsb_Rot,SIGNAL(valueChanged(double)),SLOT(transfoSpinChanged()));
	connect(ui->dsb_s,SIGNAL(valueChanged(double)),SLOT(transfoSpinChanged()));

    connect(ui->actionQuit, SIGNAL(triggered()), this, SLOT(close()));
    connect(ui->actionLoadIMG, SIGNAL(triggered()), this, SLOT(loadImage()));
    connect(ui->actionSaveIMG, SIGNAL(triggered()), this, SLOT(saveImage()));
	connect(ui->actionDrawSobel, SIGNAL(triggered()), this, SLOT(drawSobel()));
	connect(ui->actionLissage, SIGNAL(triggered()), this, SLOT(smoothImg()));
	connect(ui->actionDownscale, SIGNAL(triggered()), this, SLOT(downscaleImg()));
	connect(ui->actionSeuil, SIGNAL(triggered()), this, SLOT(thresholdImg()));

	connect(ui->actionLoadCSG, SIGNAL(triggered()), this, SLOT(loadCSG()));
	connect(ui->actionSaveCSG, SIGNAL(triggered()), this, SLOT(saveCSG()));
	connect(ui->actionAppendCSG, SIGNAL(triggered()), this, SLOT(appendCSG()));
	connect(ui->actionClearCSG, SIGNAL(triggered()), this, SLOT(clearCSG()));

	connect(ui->actionClone, SIGNAL(triggered()), this, SLOT(clone()));
	connect(ui->actionSwapLRRoot, SIGNAL(triggered()), this, SLOT(swapLRRoot()));
	connect(ui->actionUnjoinRoot, SIGNAL(triggered()), this, SLOT(unjoinRoot()));

	connect(ui->currentNode, SIGNAL(valueChanged(int)), this, SLOT(currentNodeChanged(int)));
	connect(ui->checkBox_drawCurrent, SIGNAL(toggled(bool)), this, SLOT(updateTreeRender()));

	connect(ui->id_filsGauche, SIGNAL(valueChanged(int)), this, SLOT(updateTreeRender()));
	connect(ui->id_filsDroit, SIGNAL(valueChanged(int)), this, SLOT(updateTreeRender()));


	m_graphTextEdit = new GraphTextEdit();
	m_graphTextEdit->show();
	connect(m_graphTextEdit,SIGNAL(copyAvailable(bool)),SLOT(nodeTextSelected(bool)));
    update();
}

MainWindow::~MainWindow()
{
	delete ui;
}


void MainWindow::updateTreeRender()
{
	drawTree();
	m_render->update();
}



void MainWindow::closeEvent(QCloseEvent* /*event*/)
{
	m_graphTextEdit->close();
}

void MainWindow::createPrimtive()
{
        applyTransfo();
	int prim =  ui->prim_type->currentIndex();
	int sides = ui->nb_sides->value();

    if(prim == 0)       // Disk
    {
       m_currentNode = std::make_shared<CsgDisk>();
    }
    else                // Polygon
    {
       m_currentNode = std::make_shared<CsgRegularPolygon>(sides);
    }
    m_tree.addPrimitive(std::static_pointer_cast<CsgPrimitive>(m_currentNode));
    updateMaximumIds();


    if(!ui->currentNode->isEnabled()) // First add
    {
        ui->currentNode->setEnabled(true);
    }
    ui->currentNode->setValue(m_currentNode->id()); // recupere l'id du noeud cree
    if(ui->currentNode->maximum() == 1)
    {
        updateTextGraph();
        updateTreeRender();
    }
}


void MainWindow::createOperation()
{
    applyTransfo();
	int typeOp = ui->Operation->currentIndex();
	int left = ui->id_filsGauche->value();
	int right = ui->id_filsDroit->value();

	std::cout << "createOperation  ";
	std::cout << "type "<< typeOp;
	std::cout << " child: "<< left << " & "<< right;
	std::cout << std::endl;
    if(left == right) return message("Can't create an operation with the same two nodes.",
                                     "Error", QMessageBox::Warning);

    assert(left != right);

    unsigned int id = 0;

    switch(typeOp)
	{
    case 0:
        id = m_tree.joinPrimitive(Union, left, right);
        break;
    case 1:
        id = m_tree.joinPrimitive(Intersection, left, right);
        break;
    case 2:
        id = m_tree.joinPrimitive(Difference, left, right);
        break;
    default:
        std::cerr << "unknown operation" << std::endl;
        return;
        break;
    };

    if(id == -6) return message("The left node isn't a root node.", "Error", QMessageBox::Warning);
    if(id == -7) return message("The right node isn't a root node.", "Error", QMessageBox::Warning);

    // mettre a jour ui->currentNode ui->id_filsGauche ui->id_filsDroit

    updateMaximumIds();

    // mettre a jour ui->currentNode ui->id_filsGauche ui->id_filsDroit
    ui->currentNode->setValue(id);


    //updateTreeRender();
    //updateTextGraph();

}


void MainWindow::applyTransfo()
{
    if(m_currentNode)
    {
        m_transfo = m_currentNode->getTransfo();
        resetTransfoWidgets();
    }
}


void MainWindow::resetTransfoWidgets()
{
	m_stopSignal=true;
	ui->translationX->setValue(0);
	ui->translationY->setValue(0);
	ui->scale->setValue(0);
	ui->rotation->setValue(0);
	m_stopSignal=false;
	transfoSliderChanged();

}

void MainWindow::message(QString message, QString title, QMessageBox::Icon flag)
{
    QMessageBox mb(flag,
                   title, message,
                   QMessageBox::Ok,
                   this,
                   Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint | Qt::FramelessWindowHint);
    mb.exec();
}


void MainWindow::resetTransfo()
{
    //m_currentNode->setTransform(m_transfo);
    resetTransfoWidgets();
}

void MainWindow::transfoChanged()
{
    Matrix33d temp = m_transfo;
    temp.translate(ui->dsb_tx->value(), ui->dsb_ty->value());
    temp.rotate(Matrix33d::radians(ui->dsb_Rot->value()));
    temp.scale(ui->dsb_s->value(), ui->dsb_s->value());

    if(m_currentNode)
    {
        m_currentNode->setTransform(temp);
        updateTreeRender();
    }
}

#define S1_FACTOR 20.0
#define S2_FACTOR 40.0


void MainWindow::transfoSliderChanged()
{
	if (m_stopSignal)
		return;

	m_stopSignal = true;

    ui->dsb_tx->setValue(ui->translationX->value()/S2_FACTOR);
    ui->dsb_ty->setValue(ui->translationY->value()/S2_FACTOR);
	ui->dsb_Rot->setValue(ui->rotation->value());

	int ss = ui->scale->value();
	if (ss>=0)
		ui->dsb_s->setValue(1.0+ss/S1_FACTOR);
	else
		ui->dsb_s->setValue(1.0/(1.0-ss/S2_FACTOR));

	m_stopSignal = false;

	transfoChanged();
}

void MainWindow::transfoSpinChanged()
{
	if (m_stopSignal)
		return;

	m_stopSignal = true;

	ui->translationX->setValue(ui->dsb_tx->value());
	ui->translationY->setValue(ui->dsb_ty->value());
	ui->rotation->setValue(ui->dsb_Rot->value());

	double ss = ui->dsb_s->value();
	if (ss>=1.0)
		ui->scale->setValue((ss-1.0)*S1_FACTOR);
	else
		ui->scale->setValue((1.0-1.0/ss)*S2_FACTOR);

	m_stopSignal = false;

	transfoChanged();
}

#undef S1_FACTOR
#undef S2_FACTOR


void MainWindow::loadImage()
{
	QString fileName = QFileDialog::getOpenFileName(this,tr("Open Image"), QDir::currentPath(),tr("pgm (*.pgm);;all (*.*)"));
	if (!fileName.isEmpty())
	{
		std::string strFN = fileName.toStdString();
		// load texture
		m_render->loadTexture(strFN);
		update();
	}
}

void MainWindow::saveImage()
{
	QString fileName = QFileDialog::getSaveFileName(this,
									tr("Save Image"), QDir::currentPath(),tr("pgm (*.pgm);;all (*.*)"));
	if (!fileName.isEmpty())
	{
		std::string strFN = fileName.toStdString();
                m_render->getImg().save_to(strFN);
//		m_render->getImg().savePGMascii(strFN);
	}
}


void MainWindow::drawSobel()
{
	m_render->toggleSobel();
}

void MainWindow::smoothImg()
{
    m_render->smoothImg();
}

void MainWindow::downscaleImg()
{
    m_render->downscaleImg();
}

void MainWindow::thresholdImg()
{
    m_render->thresholdImg();
}


void MainWindow::loadCSG()
{
	QString fileName = QFileDialog::getOpenFileName(this,
									tr("Open File"), QDir::currentPath(),tr("csg (*.csg);;all (*.*)"));
	if (!fileName.isEmpty())
	{
		std::string strFN = fileName.toStdString();

        m_tree.clear();
        try
        {
            m_tree.loadFromFile(strFN);
        }
        catch(const std::exception& e)
        {
                return message(e.what(), "Loading Error", QMessageBox::Warning);
        }


        updateMaximumIds();
        updateTextGraph();
        updateTreeRender();
	}
}

// same as load but no clear before readind the tree
void MainWindow::appendCSG()
{
	QString fileName = QFileDialog::getOpenFileName(this,
									tr("Open File"), QDir::currentPath(),tr("csg (*.csg);;all (*.*)"));
	if (!fileName.isEmpty())
	{
		std::string strFN = fileName.toStdString();

        try
        {
            m_tree.loadFromFile(strFN);
        }
        catch(const std::exception& e)
        {
                return message(e.what(), "Loading Error", QMessageBox::Warning);
        }

        updateMaximumIds();
        updateTextGraph();
		updateTreeRender();

	}
}

void MainWindow::saveCSG()
{
	QString fileName = QFileDialog::getSaveFileName(this,
									tr("Save File"), QDir::currentPath(),tr("csg (*.csg);;all (*.*)"));
	if (!fileName.isEmpty())
	{
		std::string strFN = fileName.toStdString();
        try
        {
            m_tree.saveToFile(strFN);

        }
        catch(const std::exception& e)
        {
                return message(e.what(), "Saving Error", QMessageBox::Warning);
        }
    }
}

void MainWindow::clearCSG()
{
    m_currentNode.reset();
    m_tree.clear();
    updateMaximumIds();
    updateTextGraph();
	updateTreeRender();
}


void MainWindow::clone()
{
    if(!m_currentNode) return;
    m_tree.clone(m_currentNode->id());
    updateMaximumIds();
    updateTextGraph();
	updateTreeRender();

}


void MainWindow::drawTree()
{
	m_render->clean();

    auto t1 = high_resolution_clock::now();
    m_tree.drawInImage( m_render->getImg() );
    auto t2 = high_resolution_clock::now();
    std::cout << "Drawing tree: " << duration_cast<milliseconds>( t2 - t1 ).count() << " ms\n";

    if (ui->checkBox_drawCurrent->isChecked() && m_currentNode.get()!=NULL)
	{
		// OPTION: trace le noeud courant dans l'image de m_render
		// VOTRE CODE ICI

		m_render->setBBDraw(true);
        m_bb = m_currentNode->getBBox();
	}
	else
	{
		m_render->setBBDraw(false);
	}

// trace les 2 fils de l'operation avec 2 niveau de gris pour vizu
/*	int idf = ui->id_filsGauche->value();
	if (idf != 0)
	{
		CsgNode* fNode = m_tree.fromId(idf);
		if (fNode->isRoot())
			m_tree.drawInImage(fNode, m_render->getImg(),175);
	}

	idf = ui->id_filsDroit->value();
	if (idf != 0)
	{
		CsgNode* fNode = m_tree.fromId(idf);
		if (fNode->isRoot())
			m_tree.drawInImage(fNode, m_render->getImg(),200);
	}
*/
    m_render->updateDataTexture();
}


void MainWindow::nodeTextSelected(bool sel)
{
	if (!sel)
		return;

	QTextCursor cursor = m_graphTextEdit->textCursor();
	std::string nodeLabel = cursor.selectedText().toStdString();

	if (nodeLabel.size() !=5)
		return;

	// get id from label string
	std::string strId = nodeLabel.substr(2,4);
	std::stringstream ss(strId);
	unsigned int id;
	ss >> id;

	ui->currentNode->setValue(id);

	if (m_graphTextEdit->pressed() == 'l')
	{
		ui->id_filsGauche->setValue(id);
	}
	else if (m_graphTextEdit->pressed() == 'r'	)
	{
		ui->id_filsDroit->setValue(id);
	}
	else
	{
		ui->currentNode->setValue(id);
	}

}

void MainWindow::updateMaximumIds()
{

    if(m_tree.getMaxAvailableId() > 0)
    {
        ui->currentNode->setMinimum(1);
        ui->id_filsDroit->setMinimum(1);
        ui->id_filsGauche->setMinimum(1);
        ui->currentNode->setMaximum(m_tree.getMaxAvailableId());
        ui->id_filsDroit->setMaximum(m_tree.getMaxAvailableId());
        ui->id_filsGauche->setMaximum(m_tree.getMaxAvailableId());
        ui->currentNode->setEnabled(true);
        ui->id_filsDroit->setEnabled(true);
        ui->id_filsGauche->setEnabled(true);
    }
    else
    {
        ui->currentNode->setEnabled(false);
        ui->id_filsDroit->setEnabled(false);
        ui->id_filsGauche->setEnabled(false);
        ui->currentNode->setMinimum(-1);
        ui->id_filsDroit->setMinimum(-1);
        ui->id_filsGauche->setMinimum(-1);
        ui->currentNode->setMaximum(-1);
        ui->id_filsDroit->setMaximum(-1);
        ui->id_filsGauche->setMaximum(-1);
    }
}


void MainWindow::updateTextGraph()
{
	// update Graph in TextWindow
	m_graphTextEdit->clear();
    std::string str = m_tree.asciiArtGraph();
    m_graphTextEdit->appendPlainText(str.c_str());
}


void MainWindow::currentNodeChanged(int id)
{
    if(!ui->currentNode->isEnabled()) return;
    m_currentNode = m_tree.fromId(id);
    if(m_currentNode)
    {
        m_transfo = m_currentNode->getTransfo();
        m_stored_center = m_currentNode->getBBox().center();
    }
    resetTransfoWidgets();
    updateTextGraph();
}


void MainWindow::swapLRRoot()
{
    if(m_currentNode.get() == NULL) return;
    if(!m_currentNode->isOperation()) return message(
                "The current node isn't an operation node",
                "Error", QMessageBox::Warning);
    static_cast<CsgOperation*>(m_currentNode.get())->swap();
	updateTextGraph();
	updateTreeRender();
}



void MainWindow::unjoinRoot()
{
    if(!m_currentNode) return;
    if(!m_currentNode->isOperation()) return message(
                "The current node isn't an operation node",
                "Error", QMessageBox::Warning);
    try{
        m_tree.unjoin(m_currentNode->id());

    }
    catch(const std::exception& e)
    {
        return message(e.what(), "Unjoin Error", QMessageBox::Warning);
    }

    updateMaximumIds();
	updateTextGraph();
	updateTreeRender();
}


GraphTextEdit::GraphTextEdit()
{
	this->resize(800,800);
	this->setWindowTitle("CSG-Graph");
	this->setReadOnly(true);
	this->setWordWrapMode(QTextOption::NoWrap);

	QFont font = QFont ("Courier");
	font.setStyleHint(QFont::TypeWriter);
	font.setPointSize(11);
	font.setFixedPitch (true);
	this->setFont(font);
}

