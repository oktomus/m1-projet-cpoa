cmake_minimum_required(VERSION 3.0)

project(InterfaceCSG LANGUAGES C CXX)

find_package(OpenGL REQUIRED)
find_package(Qt5Widgets REQUIRED)
find_package(Qt5OpenGL REQUIRED)

find_package(OpenMP)
if (OPENMP_FOUND)
    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
endif()

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)

file ( GLOB HEADERS *.h)
file ( GLOB SOURCES *.cpp)

add_executable(${PROJECT_NAME} MACOSX_BUNDLE ${HEADERS} ${SOURCES})

target_include_directories(${PROJECT_NAME} PRIVATE
	${CMAKE_CURRENT_SOURCE_DIR}
	${CMAKE_CURRENT_SOURCE_DIR}/../vectorMatrix
	${CMAKE_CURRENT_SOURCE_DIR}/../image
	${CMAKE_CURRENT_SOURCE_DIR}/../csg
	${CMAKE_CURRENT_SOURCE_DIR}/../particle)


target_link_libraries(${PROJECT_NAME} Qt5::Widgets Qt5::OpenGL csg vector_matrix particle image ${OPENGL_LIBRARIES})

#target_link_libraries(${PROJECT_NAME} csg vector_matrix image particle Qt5::Widgets Qt5::OpenGL ${OPENGL_LIBRARIES})
