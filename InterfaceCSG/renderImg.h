#ifndef RENDERIMG_H
#define RENDERIMG_H

#include <QGLWidget>
#include <QTimer>
#include <memory>
#include <queue>
#include <vector>
#include "image2D.h"
#include "gradientsobel.h"
#include "particle.h"

//#include "image2grey.h"
//#include "vec2f.h"
//#include "particle.h"
#include "boundingBox.h"

//forward declaration
class BoundingBox;

class RenderImg : public QGLWidget
{
    Q_OBJECT

    QTimer *m_timer;
    Image2Grey m_img;
    std::unique_ptr<GradientSobel> m_grad;
    BoundingBox & m_BB;
    std::priority_queue<Particle>
    m_particles;
    float globalTime = 0.0;

public:
    RenderImg(BoundingBox & bb, QWidget *parent = 0);
	~RenderImg();

	void loadTexture(const std::string& filename);
	void updateDataTexture();

	Image2Grey & getImg();
	unsigned int getWidth();
	unsigned int getHeight();

	/**
	 * @brief clean image
	 */
	void clean();


   /**
	* @brief update sobel image
	*/
   void toggleSobel();

   /**
     * Smooth the image
     */
   void smoothImg();

   /**
     * Downscale with a factor of 2 the image
     */
   void downscaleImg();

   /**
     * Apply a 128 threshold on the image
     */
   void thresholdImg();

   /**
	* @brief setif BB has to be drawn
	* @param v
	*/
   void setBBDraw(bool v) { m_BBdraw = v;}

protected slots:
	void animate();

protected:
	/// Qt callbacks
	void initializeGL();
	void paintGL();
	void resizeGL(int width, int height);
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	void keyPressEvent(QKeyEvent* event);
	void keyReleaseEvent(QKeyEvent* event);


	/// transform win coordinate in texel image coordinate
	void coordInTexture(QMouseEvent *event, int& x, int& y);

	/// transform img coord to GL [-1,+1]
	inline float xImg2GL(float x)
	{
		return -1.0f + (2.0f*x)/float(m_widthTex-1);
	}

	/// transform img coord to GL [-1,+1]
	inline float yImg2GL(float y)
	{
		return 1.0f - (2.0f*y)/float(m_heightTex-1);// minus because of GL is bottom to up and image up to boytom
	}

        //Vec2V points[50];



	/// Texture information
	GLuint m_texture;
	int m_widthTex;
	int m_heightTex;
	unsigned char* m_ptrTex;

	/// GL Widget sizes
	int m_winW;
	int m_winH;
	int m_winS;

	/// for mouse move
	QPoint m_lastPos;
	/// key modifier state (shift/ctrl/...)
	int m_state_modifier;

	bool m_drawSobel;

	bool m_BBdraw;
//	BoundingBox& m_BB;


	void drawBB(const BoundingBox& bb);

	void drawSobel();

//	ici les declaration de:
//   - l'image a niveau de gris
//	 - l'image gradiant'

//	 la fontaine de particule
//	Fountain fountain;
};

#endif // RENDERIMG_H
