cmake_minimum_required(VERSION 3.0)

if(MSVC)
	add_definitions("-D_USE_MATH_DEFINES")
else()
        add_compile_options(-std=c++11)
endif()


project(Projet_Particules LANGUAGES CXX)

set(GT ${CMAKE_CURRENT_SOURCE_DIR}/GT)

add_subdirectory(vectorMatrix)
add_subdirectory(image)
add_subdirectory(csg)
add_subdirectory(particle)
add_subdirectory(InterfaceCSG)

# Copy test files

file(GLOB pgmTestFiles "image/tests/*.ascii.pgm")
file(GLOB ppmTestFiles "image/tests/*.ascii.ppm")
list(APPEND imageTestFiles ${pgmTestFiles})
list(APPEND imageTestFiles ${ppmTestFiles})

foreach(file ${imageTestFiles})
    get_filename_component(filename ${file} NAME)
    configure_file(${file} ${CMAKE_CURRENT_BINARY_DIR}/test_files/${filename} COPYONLY)
    message(STATUS "Copy ${filename} into test_files")
endforeach()

message(STATUS "Test files are located in ${CMAKE_CURRENT_BINARY_DIR}/test_files/") 
