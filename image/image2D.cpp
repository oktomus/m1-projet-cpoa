#include "image2D.h"

/*

                                                 
   ▀                                 ▄▄▄▄      █ 
 ▄▄▄    ▄▄▄▄▄   ▄▄▄    ▄▄▄▄   ▄▄▄   ▀   ▀█  ▄▄▄█ 
   █    █ █ █  ▀   █  █▀ ▀█  █▀  █      ▄▀ █▀ ▀█ 
   █    █ █ █  ▄▀▀▀█  █   █  █▀▀▀▀    ▄▀   █   █ 
 ▄▄█▄▄  █ █ █  ▀▄▄▀█  ▀█▄▀█  ▀█▄▄▀  ▄█▄▄▄▄ ▀█▄██ 
                       ▄  █                      
                        ▀▀                       
 */

void Image2Grey::save_to(std::string file)
{
    std::ofstream wf(file);
    if(!wf.is_open()) throw std::runtime_error("Can't open file for writing");
    wf << "P2\n";
    wf << width << " " << height << "\n";
    wf << "255\n";

    for(size_t y = 0; y < height; y++)
    {
        for(size_t x = 0; x < width; ++x)
        {
            wf << (int) ((*this)(x, y)) << " ";
        }
        wf << "\n";
    }

    wf.close();
    

}

void Image2Grey::seuil(const unsigned char& value)
{
    for(size_t y = 0; y < height; ++y)
    {
        for(size_t x = 0; x < width; ++x)
        {
            if((*this)(x, y) < value)
                (*this)(x, y) = 0;
            else
                (*this)(x, y) = 255;
        }
    }

}

void Image2RGB::save_to(std::string file)
{
    std::ofstream wf(file);
    if(!wf.is_open()) throw std::runtime_error("Can't open file for writing");
    wf << "P3\n";
    wf << width << " " << height << "\n";
    wf << "255\n";

    for(size_t y = 0; y < height; y++)
    {
        for(size_t x = 0; x < width; ++x)
        {
            wf << (int) ((*this)(x, y)[0]) << " ";
            wf << (int) ((*this)(x, y)[1]) << " ";
            wf << (int) ((*this)(x, y)[2]) << " ";
        }
        wf << "\n";
    }

    wf.close();
    

}
