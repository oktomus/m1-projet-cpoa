#ifndef __IMAGE2D__
#define __IMAGE2D__

#include <array>
#include <algorithm>
#include <string>
#include <cmath>
#include <fstream>
#include <sstream>
#include <iostream>
#include <stdexcept>
#include <memory>
#include "vector.h"

/*

                                                 
   ▀                                 ▄▄▄▄      █ 
 ▄▄▄    ▄▄▄▄▄   ▄▄▄    ▄▄▄▄   ▄▄▄   ▀   ▀█  ▄▄▄█ 
   █    █ █ █  ▀   █  █▀ ▀█  █▀  █      ▄▀ █▀ ▀█ 
   █    █ █ █  ▄▀▀▀█  █   █  █▀▀▀▀    ▄▀   █   █ 
 ▄▄█▄▄  █ █ █  ▀▄▄▀█  ▀█▄▀█  ▀█▄▄▀  ▄█▄▄▄▄ ▀█▄██ 
                       ▄  █                      
                        ▀▀                       

*/

namespace utils {

    /**
      * Safely get a line from a file and ignores the differences
      * between CLRF, RF and LF
      *
      * @param  The input stream
      * @param  The output string
      * @return The modified stream
     */
    inline std::istream& getline(std::istream& is, std::string& out)
    {
        out.clear();

        std::istream::sentry sentry(is, true);
        std::streambuf* sbuf = is.rdbuf();

        while(true)
        {
            // Get next char
            int ch = sbuf->sbumpc();
            switch (ch) {
            case '\n':
                // End of line
                return is;
            case '\r':
                // End of line, but not in the right format
                // If the next char is \n, then go to it
                if(sbuf->sgetc() == '\n')
                    sbuf->sbumpc();
                return is;
            case EOF:
                // Last line of a file
                if(out.empty())
                    is.setstate(std::ios::eofbit);
                return is;
            default:
                out += (char)ch;
            }
        }
    }

};
//////////////////////////////////////
/// Definition
//////////////////////////////////////

/**
  * A 2D image of given pixel type
  * @param P    The type for pixels
  */
template <class P>
class Image2D
{
public:

    /**
      * Only constructor allowed
      *
      * Specify the size
      */
    Image2D(const size_t& pwidth, const size_t& pheight):
        width(width_), height(height_), width_(pwidth), height_(pheight),
        pixels_(new P[pwidth * pheight], std::default_delete<P[]>())
    {
    }

    Image2D(const Image2D& other):
        width(width_), height(height_), width_(other.width), height_(other.height),
        pixels_(other.pixels_)
    {
    }

    /**
      * Destroy the pixels
      */
    ~Image2D()
    {
        pixels_.reset();
    }

    /**
      * Public read-only accessor to the width
      */
    const size_t &width;
    
    /**
      * Public read-only accessor to the height
      */
    const size_t &height;

    /**
      * Return a pointer on pixels values
      *
      * @return     A Pointer of pixel type
      */
    P * get_pixels()
    {
        return pixels_.get();
    }

    /**
      * Swap with another image
      *
      * Swap anything including pixels and size
      *
      * @param other    The other image
      */
    void swap(Image2D & other)
    {
        std::swap(width_, other.width_);
        std::swap(height_, other.height_);
        pixels_.swap(other.pixels_);
    }

    P& operator()(const size_t& x, const size_t& y)
    {
        return pixels_.get()[y*width_ + x];
    }

    const P& operator()(const size_t& x, const size_t& y) const
    {
        return pixels_.get()[y*width_ + x];
    }

    /**
      * Assign operator
      *
      *
      * @param other    the other image
      * @return The new image
      */
    Image2D& operator=(Image2D other)
    {
        swap(other);
        return *this;
    }

protected:

    /**
      * Image width and height
      */
    size_t width_, height_;

    /**
      * Image's pixels
      */
    std::shared_ptr<P>  pixels_;

};

/**
  * 2D Grayscale image
  */
class Image2Grey : public Image2D<unsigned char>
{
public:

    using Image2D<unsigned char>::Image2D;

    /**
      * Save the image into a PGM file
      *
      * @param file     The output ascii file
      */
    void save_to(std::string file);

    /**
      * Apply a threshold based on the given value
      *
      * @param value    Threshold value
      */
    void seuil(const unsigned char& value);

    /**
      * Constructs an image by reading any PGM file
      *
      * http://people.sc.fsu.edu/%7Ejburkardt/data/pgma/pgma.html
      *
      * @param file     The image file path
      */
    Image2Grey(const std::string & file) : Image2Grey(0, 0)
    {
        std::ifstream ifs(file, std::ios_base::in);

        if (!ifs.is_open())
            throw std::runtime_error("Unable to open the image file.");

        std::string line;

        utils::getline(ifs, line);

        if (line.compare("P2") != 0)
            throw std::runtime_error("Wrong image format file.");

        int read;
        int fileWidth = -1, fileHeight = -1, state = 0, max_white = -1;

        // Read Header
        while(state < 3 && utils::getline(ifs, line))
        {
            if(line[0] == '#') continue; // Ignore comments
            std::stringstream linestream(line);

            while(linestream >> read)
            {
                if(state == 0) fileWidth = read;
                else if(state == 1) fileHeight = read;
                else if(state == 2) max_white = read;
                ++state;
            }
        }

        this->width_ = fileWidth; 
        this->height_ = fileHeight;
        unsigned int size = fileWidth * fileHeight;
        this->pixels_.reset();
        this->pixels_ = std::shared_ptr<unsigned char>(
                    new unsigned char[size],
                    std::default_delete<unsigned char[]>());

        double ratio = 255.0 / max_white;
        size_t y = 0, x = 0;
        // Read pixels
        std::ofstream wf("test_write_mone.txt");
        while(y < height)
        {
            x = 0;
            while(x < width && ifs >> read)
            {
                wf << read;
                (*this)(x, y) = ratio * read;
                ++x;
            }
            wf << "\n";
            ++y;
        }
        wf.close();

        ifs.close();
    }


    /**
      * Downscale the given image with a factor of 2 and return it
      *
      * @param image    The source image
      * @return         The image downscaled
      */
    static Image2Grey downscale(const Image2Grey& image)
    {
        Image2Grey small(image.width / 2, image.height / 2);

        size_t x_source = 0, y_source = 0;
        int border_right, border_left, border_bottom, border_top;
        for(size_t y = 0; y < small.height; ++y)
        {
            y_source = y * 2;

            if(y_source == image.height - 1)
                border_bottom = y_source;
            else
                border_bottom = y_source + 1;

            if(y_source == 0)
                border_top = y_source;
            else
                border_top = y_source - 1;

            for(size_t x = 0; x < small.width; ++x)
            {
                x_source = x * 2;
                if(x_source == image.width - 1)
                    border_right = x_source;
                else
                    border_right = x_source + 1;

                if(x_source == 0)
                    border_left = x_source;
                else
                    border_left = x_source - 1;

                small(x, y) = (
                        image(x_source, y_source) +
                        image(border_right, y_source) +
                        image(border_right, border_top) +
                        image(border_right, border_bottom) +
                        image(border_left, y_source) +
                        image(border_left, border_top) +
                        image(border_left, border_bottom) +
                        image(x_source, border_bottom) +
                        image(x_source, border_top)
                        ) / 9;


            }
        }

        return small;

    }

    /**
      * Smooth the given image and return a new one.
      *
      * The scale represents the radius of the window used
      * to calculate each pixel
      *
      * @param image    The source image
      * @param scale    The window size (minimum 1)
      * @return         The image downscaled
      */
    static Image2Grey smooth(const Image2Grey& image, const int scale)
    {
        Image2Grey smoothed(image.width, image.height);

        size_t pixel_count = pow((2 * scale) + 1, 2);
        int x_bound, y_bound;
        for(size_t y = 0; y < smoothed.height; ++y)
        {
            for(size_t x = 0; x < smoothed.width; ++x)
            {
                unsigned char p(0);

                for(int i = -scale; i <= scale; ++i)
                {
                    y_bound = y + i;
                    if (y_bound < 0) y_bound = 0;
                    else if (y_bound > image.height - 1) y_bound = image.height - 1;
                    for(int j = -scale; j <= scale; ++j)
                    {
                        x_bound = x + j;
                        if (x_bound < 0) x_bound = 0;
                        else if (x_bound > image.width - 1) x_bound = image.width - 1;

                        p += (image(x_bound, y_bound) / pixel_count);
                    }
                }

                smoothed(x, y) = p;
            }
        }

        return smoothed;

    }


};

/**
  * 2D RGB image
  */
class Image2RGB : public Image2D<Vec3uc>
{
public:

    using Image2D<Vec3uc>::Image2D;

    /**
      * Save the image into a PPM file
      *
      * @param file     The output ascii file
      */
    void save_to(std::string file);

    /**
      * Read any PPM file and returns an image
      *
      * https://www.cs.swarthmore.edu/~soni/cs35/f13/Labs/extras/01/ppm_info.html
      *
      * @param file     The image file
      * @returne        An Image2RGB Object
      */
    Image2RGB(const std::string & file) : Image2RGB(0, 0)
    {
        std::ifstream ifs(file);

        if (!ifs.is_open())
            throw std::runtime_error("Unable to open the image file.");

        std::string line;

        utils::getline(ifs, line);

        if (line.compare("P3") != 0)
            throw std::runtime_error("Wrong image format file.");

        int read;
        int width = -1, height = -1, state = 0, max = -1;

        // Read Header
        while(state < 3 && utils::getline(ifs, line))
        {
            if(line[0] == '#') continue; // Ignore comments
            std::stringstream linestream(line);

            while(linestream >> read)
            {
                if(state == 0) width = read;
                else if(state == 1) height = read;
                else if(state == 2) max = read;
                ++state;
            }
        }

        this->width_ = width;
        this->height_ = height;
        this->pixels_.reset();
        this->pixels_ = std::shared_ptr<Vec3uc>(
                    new Vec3uc[this->width * this->height],
                std::default_delete<Vec3uc[]>());

        double ratio = 255.0 / max;
        int y = 0, x = 0;
        // Read pixels
        while(y < height)
        {
            x = 0;
            while(x < width && ifs >> read)
            {
                (*this)(x, y)[0] = ratio * read;
                ifs >> read;
                (*this)(x, y)[1] = ratio * read;
                ifs >> read;
                (*this)(x, y)[2] = ratio * read;
                ++x;
            }
            ++y;
        }

        ifs.close();

    }

};

#endif
