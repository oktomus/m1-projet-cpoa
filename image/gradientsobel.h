#ifndef __GRADIENT_SBOEL__
#define __GRADIENT_SBOEL__

#include "image2D.h"
#include "vector.h"

/*
                          █    ▀                    ▄   
  ▄▄▄▄   ▄ ▄▄   ▄▄▄    ▄▄▄█  ▄▄▄     ▄▄▄   ▄ ▄▄   ▄▄█▄▄ 
 █▀ ▀█   █▀  ▀ ▀   █  █▀ ▀█    █    █▀  █  █▀  █    █   
 █   █   █     ▄▀▀▀█  █   █    █    █▀▀▀▀  █   █    █   
 ▀█▄▀█   █     ▀▄▄▀█  ▀█▄██  ▄▄█▄▄  ▀█▄▄▀  █   █    ▀▄▄ 
  ▄  █                                                  
   ▀▀                                                   
               █             ▀▀█   
  ▄▄▄    ▄▄▄   █▄▄▄    ▄▄▄     █   
 █   ▀  █▀ ▀█  █▀ ▀█  █▀  █    █   
  ▀▀▀▄  █   █  █   █  █▀▀▀▀    █   
 ▀▄▄▄▀  ▀█▄█▀  ██▄█▀  ▀█▄▄▀    ▀▄▄ 

 */

#define WIN_Y_THICK 2
#define WIN_X_THICK 2

/**
  * Bounce vectors on each pixel of a greyscale image
  */
class GradientSobel : public Image2D<Vec2f>
{
public:

    using Image2D<Vec2f>::Image2D;

    /**
      * Returns the filter results of a given image
      *
      * @param source   The source greyscale image
      */
    GradientSobel(const Image2Grey& source);

private:

    /**
      * Calculate the vectors of the given image
      *
      * Method called when creating the object
      *
      * @param image    The source image
      */
    void process(const Image2Grey& image);

    const float horizontalWindow[WIN_Y_THICK*2+1][WIN_X_THICK*2+1] = {
        {1.f, 2.f, 0.f, -2.f, -1.f},
        {4.f, 8.f, 0.f, -8.f, -4.f},
        {6.f, 12.f, 0.f, -12.f, -6.f},
        {4.f, 8.f, 0.f, -8.f, -4.f},
        {1.f, 2.f, 0.f, -2.f, -1.f}
    };

    const float verticalWindow[WIN_Y_THICK*2+1][WIN_X_THICK*2+1] = {
        {1.f, 4.f, 6.f, 4.f, 1.f},
        {2.f, 8.f, 12.f, 8.f, 2.f},
        {0.f, 0.f, 0.f, 0.f, 0.f},
        {-2.f, -8.f, -12.f,-8.f, -2.f},
        {-1.f, -4.f, -6.f, -4.f, -1.f},
    };

};

#endif
