#include <iostream>

#include "image2D.h"
#include "gradientsobel.h"

#include <fstream>
#include <string>
#include <map>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

std::map<std::string, std::string> paths;

TEST(Image2D, testSize)
{
    Image2Grey img(500, 400);

    ASSERT_EQ(img.width, 500);
    ASSERT_EQ(img.height, 400);
}

TEST(Image2D, testPixAffect)
{
    Image2Grey img(10, 10);

    img(3, 3) = 255;

    ASSERT_EQ(img(3, 3), 255);

    const unsigned char * pixel = img.get_pixels() + 33 * sizeof(unsigned char);
    ASSERT_EQ((*pixel), 255);
    const unsigned char * pixels = img.get_pixels();
    ASSERT_EQ(pixels[33], 255);
}

TEST(Image2D, testSwap)
{
    Image2Grey img1(10, 1);
    Image2Grey img2(1, 10);

    for(size_t x=0; x < 10; ++x)
    {
        img1(x, 0) = x * 100;
    }
    
    for(size_t y=0; y < 10; ++y)
    {
        img2(0, y) = y * 10;
    }

    img1.swap(img2);

    ASSERT_EQ(img1.width, 1);
    ASSERT_EQ(img1.height, 10);
    ASSERT_EQ(img1(0, 0), 0);
    ASSERT_EQ(img1(0, 1), 10);
    ASSERT_EQ(img1(0, 2), 20);
    ASSERT_EQ(img1(0, 9), 90);

    ASSERT_EQ(img2.width, 10);
    ASSERT_EQ(img2.height, 1);
    ASSERT_EQ(img2(0, 0), 0);
    ASSERT_EQ(img2(1, 0), 100);
    ASSERT_EQ(img2(2, 0), 200);
    ASSERT_EQ(img2(9, 0), (unsigned char)900);
}

TEST(Image2Grey, testFromSmallFile)
{
    std::string fi(paths.at("feep"));
    Image2Grey feep(fi);

    ASSERT_EQ(feep.height, 7);
    ASSERT_EQ(feep.width, 24);

    ASSERT_EQ(feep(0,0), 0); 
    ASSERT_EQ(feep(13,1), 187); 
    ASSERT_EQ(feep(23,6), 0); 
    ASSERT_EQ(feep(1,5), 51); 
}

TEST(Image2Grey, testFromBigFile)
{
    std::string fi(paths.at("lena"));
    Image2Grey feep(fi);

    ASSERT_EQ(feep.height, 1024);
    ASSERT_EQ(feep.width, 1024);

    ASSERT_EQ(feep(0,0), 168); 
    ASSERT_EQ(feep(1023, 1023), 111); 
}


TEST(Image2Grey, testWrite)
{
    std::string fi(paths.at("write_test"));
    Image2Grey small(5, 2);

    small(0, 0) = 0;
    small(1, 0) = 2;
    small(2, 0) = 4;
    small(3, 0) = 200;
    small(4, 0) = 0;
    small(0, 1) = 200;
    small(1, 1) = 4;
    small(2, 1) = 2;
    small(3, 1) = 0;
    small(4, 1) = 0;

    small.save_to(fi);

    std::ifstream ifs(fi);
    if (!ifs.is_open())
    {
        std::cout << "Can't open written file" << std::endl;
        ASSERT_EQ(0, 1);
    }

    std::string line;
    int ln = 0;
    while(std::getline(ifs, line))
    {
        if(ln == 0)
            EXPECT_EQ(line, "P2");
        else if(ln == 1)
            EXPECT_EQ(line, "5 2");
        else if(ln == 2)
            EXPECT_EQ(line, "255");
        else if(ln == 3)
            EXPECT_EQ(line, "0 2 4 200 0 ");
        else if(ln == 4)
            EXPECT_EQ("200 4 2 0 0 ", line);
        ++ln;
    }
    ASSERT_EQ(ln, 5); // 6 lines in the file (last one empty)

    ifs.close();
}

TEST(Image2Grey, testDownscale)
{
    Image2Grey base(100, 100);
    Image2Grey small = Image2Grey::downscale(base);

    ASSERT_EQ(small.width, 50);
    ASSERT_EQ(small.height, 50);

}

TEST(Image2Grey, testSmooth)
{
    Image2Grey base(100, 100);
    Image2Grey smooth = Image2Grey::smooth(base, 1);

    ASSERT_EQ(smooth.width, 100);
    ASSERT_EQ(smooth.height, 100);

}

TEST(Image2Grey, testThreshold)
{
    Image2Grey base(100, 100);
    base.seuil(100);

    ASSERT_EQ(base.width, 100);
    ASSERT_EQ(base.height, 100);

}

TEST(Image2Grey, testGradientSobel)
{
    std::string fi(paths.at("glassware_noisy"));
    Image2Grey feep(fi);

    GradientSobel gs(feep);
}

TEST(Image2Grey, testGradientSobelValues)
{
    std::string fi(paths.at("feep"));
    Image2Grey feep(fi);

    GradientSobel gs(feep);


    ASSERT_FLOAT_EQ(gs(0,0)[0], -28.56);
}

TEST(Image2RGB, testFromBigColoredFile)
{
    std::string fi(paths.at("lena_colored"));
    Image2RGB feep(fi);

    ASSERT_EQ(feep.height, 1024);
    ASSERT_EQ(feep.width, 1024);

    ASSERT_EQ(feep(0,0)[0], 254); 
    ASSERT_EQ(feep(0,0)[1], 82); 
    ASSERT_EQ(feep(0,0)[2], 82); 
    ASSERT_EQ(feep(1023, 1023)[0], 219); 
    ASSERT_EQ(feep(1023, 1023)[1], 3); 
    ASSERT_EQ(feep(1023, 1023)[2], 196); 
}

TEST(Image2RGB, testWrite)
{
    std::string fi(paths.at("write_test_ppm"));
    Image2RGB small(3, 2);

    small(0, 0)[0] = 0;
    small(0, 0)[1] = 255;
    small(0, 0)[2] = 0;

    small(1, 0)[0] = 100;
    small(1, 0)[1] = 200;
    small(1, 0)[2] = 0;

    small(2, 0)[0] = 100;
    small(2, 0)[1] = 200;
    small(2, 0)[2] = 0;

    small(0, 1)[0] = 255;
    small(0, 1)[1] = 0;
    small(0, 1)[2] = 0;

    small(1, 1)[0] = 7;
    small(1, 1)[1] = 7;
    small(1, 1)[2] = 7;

    small(2, 1)[0] = 100;
    small(2, 1)[1] = 100;
    small(2, 1)[2] = 255;

    small.save_to(fi);

    std::ifstream ifs(fi);
    if (!ifs.is_open())
    {
        std::cout << "Can't open written file" << std::endl;
        ASSERT_EQ(0, 1);
    }

    std::string line;
    int ln = 0;
    while(std::getline(ifs, line))
    {
        if(ln == 0)
            EXPECT_EQ(line, "P3");
        else if(ln == 1)
            EXPECT_EQ(line, "3 2");
        else if(ln == 2)
            EXPECT_EQ(line, "255");
        else if(ln == 3)
            EXPECT_EQ(line, "0 255 0 100 200 0 100 200 0 ");
        else if(ln == 4)
            EXPECT_EQ(line, "255 0 0 7 7 7 100 100 255 ");
        ++ln;
    }
    ASSERT_EQ(ln, 5); // 6 lines in the file (last one empty)

    ifs.close();
}

int main(int argc, char **argv)
{

        std::string path;
        if (argc != 2)
        {
            fprintf(stderr, "usage: %s <path_to_test_files_directory/ default=test_files>\n", argv[0]);
            path = std::string("test_files");
        }
        else
        {
            path = std::string(argv[1]);
        }

        if (path.at(path.length() -1) != '/') 
            path += '/';

        paths.insert(std::pair<std::string, std::string>("glassware_noisy", path + "glassware_noisy.ascii.pgm"));
        paths.insert(std::pair<std::string, std::string>("lena", path + "lena.ascii.pgm"));
        paths.insert(std::pair<std::string, std::string>("lena_colored", path + "lena_colored.ascii.ppm"));
        paths.insert(std::pair<std::string, std::string>("feep", path + "feep.ascii.pgm"));
        paths.insert(std::pair<std::string, std::string>("write_test", path + "write_test.ascii.pgm"));
        paths.insert(std::pair<std::string, std::string>("write_test_ppm", path + "write_test.ascii.ppm"));

        printf("Image test path: %s\n", path.c_str());
        printf("Image files: \n");

        for(std::map<std::string, std::string>::const_iterator it = paths.begin();
                it != paths.end(); ++it)
        {
            printf("- %s %s\n", it->first.c_str(), it->second.c_str());
        }


        testing::InitGoogleTest(&argc, argv);
        setlocale(LC_CTYPE, "");
        return RUN_ALL_TESTS();
}
