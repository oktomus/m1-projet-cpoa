#include "gradientsobel.h"

/*
                          █    ▀                    ▄   
  ▄▄▄▄   ▄ ▄▄   ▄▄▄    ▄▄▄█  ▄▄▄     ▄▄▄   ▄ ▄▄   ▄▄█▄▄ 
 █▀ ▀█   █▀  ▀ ▀   █  █▀ ▀█    █    █▀  █  █▀  █    █   
 █   █   █     ▄▀▀▀█  █   █    █    █▀▀▀▀  █   █    █   
 ▀█▄▀█   █     ▀▄▄▀█  ▀█▄██  ▄▄█▄▄  ▀█▄▄▀  █   █    ▀▄▄ 
  ▄  █                                                  
   ▀▀                                                   
               █             ▀▀█   
  ▄▄▄    ▄▄▄   █▄▄▄    ▄▄▄     █   
 █   ▀  █▀ ▀█  █▀ ▀█  █▀  █    █   
  ▀▀▀▄  █   █  █   █  █▀▀▀▀    █   
 ▀▄▄▄▀  ▀█▄█▀  ██▄█▀  ▀█▄▄▀    ▀▄▄ 

 */

GradientSobel::GradientSobel(const Image2Grey& source)
    : Image2D<Vec2f>(source.width, source.height)
{
    process(source);
}

void GradientSobel::process(const Image2Grey& image)
{

    Vec2f * cur = NULL;
    int xcalc, ycalc;
    float divide = (WIN_Y_THICK*2+1) * (WIN_X_THICK*2+1);
    for(size_t y = 0; y < height; ++y)
    {
        for(size_t x = 0; x < width; ++x)
        {
            cur = &((*this)(x, y));
            (*cur)[0] = 0; (*cur)[1] = 0;
            //*cur = Vec2f(0, 0);
            for(int i = -WIN_Y_THICK; i <= WIN_Y_THICK; ++i)
            {
                ycalc = y + i;
                if(ycalc < 0 || ycalc > height) continue;
                for(int j = -WIN_X_THICK; j <= WIN_X_THICK; ++j)
                {
                    xcalc = x + j;
                    if(xcalc < 0 || xcalc > width) continue;
                    unsigned char pix = image(xcalc, ycalc);
                    if (pix)
                        (*cur) += Vec2f{
                            horizontalWindow
                                [i + WIN_Y_THICK]
                                [j + WIN_X_THICK] *
                            pix,
                            verticalWindow
                                [i + WIN_Y_THICK]
                                [j + WIN_X_THICK] *
                            pix


                            };
                }
            }
            (*cur) /= divide;
            //std::cout << *cur << "\n";

        }
    }

}

