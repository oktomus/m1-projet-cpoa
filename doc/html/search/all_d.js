var searchData=
[
  ['save_5fto',['save_to',['../classImage2Grey.html#a9b87a75bcb00341fecdfee6f35e16296',1,'Image2Grey::save_to()'],['../classImage2RGB.html#a42bbe5a29895ae697ea2aa4739b56b42',1,'Image2RGB::save_to()']]],
  ['scale',['scale',['../classMatrix33d.html#a0a67023f1a153ca23bc6b325c564470e',1,'Matrix33d']]],
  ['scaling',['scaling',['../classMatrix33d.html#a003be6e15811ad74104cc5a13fd064e4',1,'Matrix33d']]],
  ['seuil',['seuil',['../classImage2Grey.html#ae7d6aa925adb0ce6f1c25aa1080106f7',1,'Image2Grey']]],
  ['size',['size',['../classArray.html#a322ecc1361d949943111971d2e6b975e',1,'Array']]],
  ['smooth',['smooth',['../classImage2Grey.html#a99a82d42b994ed63c98a755f38dd5fb8',1,'Image2Grey']]],
  ['swap',['swap',['../classArray.html#a610ce53a696630ad0033912ac2a4fc9a',1,'Array::swap()'],['../classImage2D.html#ac60b4390b0fc2baff5a545f1a025524e',1,'Image2D::swap()']]]
];
