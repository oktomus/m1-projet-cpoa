#include "csgOperation.h"

CsgOperation::CsgOperation(Operation opType, std::shared_ptr<CsgNode> left, std::shared_ptr<CsgNode> right)
    : opType_(opType), left_(left), right_(right),
      CsgNode()
{
    updateBBox();
}

bool CsgOperation::intersect(const Vec2f& p) const
{
    Vec2f trP(p);

    transRevert_.apply(trP);

    if(opType_ == Intersection)
        return left_->intersect(trP) && right_->intersect(trP);
    else if(opType_ == Union)
        return left_->intersect(trP) || right_->intersect(trP);
    else if(opType_ == Difference)
        return left_->intersect(trP) && !(right_->intersect(trP));
}

bool CsgOperation::isOperation() const
{
    return true;
}

void CsgOperation::swap()
{
   std::swap(left_, right_);
   updateBBox();
}

std::string CsgOperation::asciiArtNode(int offset) const
{
    std::string op;
    if(opType_ == Intersection) op = "Intersection";
    else if(opType_ == Union) op = "Union";
    else if(opType_ == Difference) op = "Difference";

    std::string res;
    res = std::string(offset, '\t') + op + " " + CsgNode::asciiArtNode(0) + " ==> \n";
    res += left_->asciiArtNode(offset + 1) + "\n";
    res += right_->asciiArtNode(offset + 1) + "\n";
    return res;
}

std::string CsgOperation::type() const
{
    return "Operation";
}

void CsgOperation::writeTo(std::ostream & out) const
{
    std::string op = "Operation";
    if(opType_ == Intersection)
        op = "Intersection";
    else if(opType_ == Union)
        op = "Union";
    else if(opType_ == Difference)
        op = "Difference";
    out << this->id() << " " << op << " " << left_->id() << " " << right_->id() << "\n";
    this->transform_.writeTo(out);
}

std::shared_ptr<CsgNode> CsgOperation::clone() const
{
    std::shared_ptr<CsgNode> clonedLeft = left_->clone();
    std::shared_ptr<CsgNode> clonedRight = right_->clone();
    std::shared_ptr<CsgNode> cloned = std::make_shared<CsgOperation>(opType_, clonedLeft, clonedRight);
    cloned->setTransform(transform_);
    return cloned;
}

void CsgOperation::updateBBox()
{
    if(opType_ == Intersection)
        bb_ =  left_->getBBox() ^ right_->getBBox();
    else if(opType_ == Union)
        bb_ = left_->getBBox() + right_->getBBox();
    else if(opType_ == Difference)
        bb_ = left_->getBBox() - right_->getBBox();

    Vec2f bottomL({bb_.xmin(), bb_.ymin()});
    Vec2f bottomR({bb_.xmax(), bb_.ymin()});
    Vec2f topR({bb_.xmax(), bb_.ymax()});
    Vec2f topL({bb_.xmin(), bb_.ymax()});
    transform_.apply(bottomL);
    transform_.apply(bottomR);
    transform_.apply(topL);
    transform_.apply(topR);

    bb_.setXmin(bottomL[0]);
    bb_.setXmax(bottomL[0]);
    bb_.setYmin(bottomL[1]);
    bb_.setYmax(bottomL[1]);

    bb_.addPoint(bottomR);
    bb_.addPoint(topL);
    bb_.addPoint(topR);

    updateParentBBox();
}
