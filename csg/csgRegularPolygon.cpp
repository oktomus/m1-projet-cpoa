#include "csgRegularPolygon.h"
#include <vector>
#include "matrix33d.h"

CsgRegularPolygon::CsgRegularPolygon(const size_t &n):
    CsgPrimitive()
{
    points_.reserve(n);

    outAngle_ = 360.0f / float(n);

    Matrix33d transpose_ = Matrix33d::rotation(Matrix33d::radians(outAngle_));
    //apothem_ = 0.5 * cos(PI / n);

    points_.push_back(Vec2f({.0f, .5f})); // Top
    for(size_t i = 1; i < n; ++i)
    {
        points_.push_back(points_[i-1]);
        transpose_.apply(points_[i]);
    }

    updateBBox();
}

bool CsgRegularPolygon::intersect(const Vec2f &p) const
{
    Vec2f c(p);

    transRevert_.apply(c);

    // Infinite line to the right
    // Only half process since the shape is symetric on x

    if(c[0] < 0)
        c[0] *= -1;

    Vec2f d{c[0] + 1000, c[1]};

    for(size_t i = 0; i < points_.size() / 2; ++i)
    {
        const Vec2f &a = points_[i];
        const Vec2f &b = points_[i + 1];
        float detcdb = det(c, d, b); // When points are aligned
        if((det(a, b, c) * det(a, b, d) < 0 && det(c, d, a) * detcdb < 0)) return true;
        else if(detcdb == 0 && c[0] < b[0]) return true;

    }

    return false;
}

std::string CsgRegularPolygon::asciiArtNode(int offset) const
{
   return std::string(offset, '\t') + "Polygon S[" +
           std::to_string(points_.size()) +
           "]" + CsgNode::asciiArtNode();
}

void CsgRegularPolygon::writeTo(std::ostream & out) const
{
    out << this->id() << " " << "Polygon " << points_.size() << "\n";
    this->transform_.writeTo(out);
}

std::shared_ptr<CsgNode> CsgRegularPolygon::clone() const
{
   std::shared_ptr<CsgNode> cloned = std::make_shared<CsgRegularPolygon>(points_.size());
   cloned->setTransform(transform_);
   return cloned;
}

void CsgRegularPolygon::updateBBox()
{

    Vec2f p = points_[0];
    transform_.apply(p);

    bb_.setXmin(p[0]);
    bb_.setXmax(p[0]);
    bb_.setYmin(p[1]);
    bb_.setYmax(p[1]);

    for(size_t i = 1; i < points_.size(); ++i)
    {
        p = points_[i];
        transform_.apply(p);
        bb_.addPoint(p);
    }
    updateParentBBox();
}

float det(const Vec2f &pa, const Vec2f &pb, const Vec2f &pc)
{
    return pa[0] * pb[1] + pb[0] * pc[1] + pc[0] * pa[1] -
            pc[0] * pb[1] - pc[1] * pa[0] - pb[0] * pa[1];

}
