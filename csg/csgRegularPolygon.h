#ifndef __CSG_REGULAR_POLYGON__
#define __CSG_REGULAR_POLYGON__

#include "csgPrimitive.h"
#include "vector.h"
#include <array>

/**
 * @brief A polygon with n>= 3 summits
 */
class CsgRegularPolygon : public CsgPrimitive
{

public:

    /**
     * @brief Create a polygon with a given number of point
     */
    CsgRegularPolygon(const size_t&);

    bool intersect(const Vec2f& p) const;

    virtual std::string asciiArtNode(int offset = 0) const;

    virtual void writeTo(std::ostream&) const;

    virtual std::shared_ptr<CsgNode> clone() const;

private:

    /**
     * @brief Summits set
     */
    std::vector<Vec2f> points_;

    /**
     * @brief Angle between each point
     */
    float outAngle_;

    /*
    /**
     * @brief Radius of the inner cirlce
    float apothem_;

    /**
     * @brief Matrix used to create the polygon with rotation on each point and to
     * transpose a point in the first traignle
    Matrix33d transpose_;
    */

protected:

    void updateBBox();

};

float det(const Vec2f& pa, const Vec2f& pb, const Vec2f& pc);

#endif
