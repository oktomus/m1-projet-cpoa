#include <iostream>

#include "boundingBox.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>


TEST(BoudningBox, testInit)
{
    EXPECT_NO_THROW(BoundingBox({-5, -5}, {5, 5}));
}

TEST(BoudningBox, testCorners)
{
    BoundingBox bo({0, -5}, {1, 0});

    EXPECT_FLOAT_EQ(bo.xmin(), 0);
    EXPECT_FLOAT_EQ(bo.ymin(), -5);
    EXPECT_FLOAT_EQ(bo.xmax(), 1);
    EXPECT_FLOAT_EQ(bo.ymax(), 0);
}

TEST(BoudningBox, testPointInside)
{
    BoundingBox bo({0, 0}, {1, 1});

    EXPECT_TRUE(bo.isPointInside(0, 0));
    EXPECT_TRUE(bo.isPointInside(1, 1));
    EXPECT_TRUE(bo.isPointInside(.5f, 0));
    EXPECT_TRUE(bo.isPointInside(0, .5f));
    EXPECT_TRUE(bo.isPointInside(.5f, .5f));

    EXPECT_FALSE(bo.isPointInside(0, -0.01));
    EXPECT_FALSE(bo.isPointInside(7, 8));
    EXPECT_FALSE(bo.isPointInside(-7, 8));
    EXPECT_FALSE(bo.isPointInside(-7, 5));
    EXPECT_FALSE(bo.isPointInside(-7, -8));
}

TEST(BoundingBox, testUnion)
{
    BoundingBox base({0, 1}, {10, 11});

    {
        BoundingBox a({5, 6}, {15, 16});
        BoundingBox ar = base + a;

        EXPECT_FLOAT_EQ(ar.xmin(), 0);
        EXPECT_FLOAT_EQ(ar.ymin(), 1);
        EXPECT_FLOAT_EQ(ar.xmax(), 15);
        EXPECT_FLOAT_EQ(ar.ymax(), 16);
    }

    {
        BoundingBox a({5, 6}, {8, 9});
        BoundingBox ar = base + a;

        EXPECT_FLOAT_EQ(ar.xmin(), 0);
        EXPECT_FLOAT_EQ(ar.ymin(), 1);
        EXPECT_FLOAT_EQ(ar.xmax(), 10);
        EXPECT_FLOAT_EQ(ar.ymax(), 11);
    }

    {
        BoundingBox a({-5, -5}, {5, 30});
        BoundingBox ar = base + a;

        EXPECT_FLOAT_EQ(ar.xmin(), -5);
        EXPECT_FLOAT_EQ(ar.ymin(), -5);
        EXPECT_FLOAT_EQ(ar.xmax(), 10);
        EXPECT_FLOAT_EQ(ar.ymax(), 30);
    }

    {
        BoundingBox a({-1, 2}, {2, 3});
        BoundingBox ar = base + a;

        EXPECT_FLOAT_EQ(ar.xmin(), -1);
        EXPECT_FLOAT_EQ(ar.ymin(), 1);
        EXPECT_FLOAT_EQ(ar.xmax(), 10);
        EXPECT_FLOAT_EQ(ar.ymax(), 11);
    }
}

TEST(BoundingBox, testIntersection)
{
    BoundingBox base({0, 0}, {10, 10});

    {
        BoundingBox a({5, 5}, {15, 15});
        BoundingBox ar = base ^ a;

        EXPECT_FLOAT_EQ(ar.xmin(), 5);
        EXPECT_FLOAT_EQ(ar.ymin(), 5);
        EXPECT_FLOAT_EQ(ar.xmax(), 10);
        EXPECT_FLOAT_EQ(ar.ymax(), 10);
    }
    {
        BoundingBox a({5, -5}, {15, 15});
        BoundingBox ar = base ^ a;

        EXPECT_FLOAT_EQ(ar.xmin(), 5);
        EXPECT_FLOAT_EQ(ar.ymin(), 0);
        EXPECT_FLOAT_EQ(ar.xmax(), 10);
        EXPECT_FLOAT_EQ(ar.ymax(), 10);
    }
    {
        BoundingBox a({-5, 5}, {15, 15});
        BoundingBox ar = base ^ a;

        EXPECT_FLOAT_EQ(ar.xmin(), 0);
        EXPECT_FLOAT_EQ(ar.ymin(), 5);
        EXPECT_FLOAT_EQ(ar.xmax(), 10);
        EXPECT_FLOAT_EQ(ar.ymax(), 10);
    }
    {
        BoundingBox a({-5, 5}, {5, 15});
        BoundingBox ar = base ^ a;

        EXPECT_FLOAT_EQ(ar.xmin(), 0);
        EXPECT_FLOAT_EQ(ar.ymin(), 5);
        EXPECT_FLOAT_EQ(ar.xmax(), 5);
        EXPECT_FLOAT_EQ(ar.ymax(), 10);
    }
    {
        BoundingBox a({-5, -5}, {5, 15});
        BoundingBox ar = base ^ a;

        EXPECT_FLOAT_EQ(ar.xmin(), 0);
        EXPECT_FLOAT_EQ(ar.ymin(), 0);
        EXPECT_FLOAT_EQ(ar.xmax(), 5);
        EXPECT_FLOAT_EQ(ar.ymax(), 10);
    }
    {
        BoundingBox a({-5, -5}, {15, 5});
        BoundingBox ar = base ^ a;

        EXPECT_FLOAT_EQ(ar.xmin(), 0);
        EXPECT_FLOAT_EQ(ar.ymin(), 0);
        EXPECT_FLOAT_EQ(ar.xmax(), 10);
        EXPECT_FLOAT_EQ(ar.ymax(), 5);
    }
    {
        BoundingBox a({5, 3}, {15, 6});
        BoundingBox ar = base ^ a;

        EXPECT_FLOAT_EQ(ar.xmin(), 5);
        EXPECT_FLOAT_EQ(ar.ymin(), 3);
        EXPECT_FLOAT_EQ(ar.xmax(), 10);
        EXPECT_FLOAT_EQ(ar.ymax(), 6);
    }
    {
        BoundingBox a({12, 5}, {20, 20});
        BoundingBox ar = base ^ a;

        EXPECT_FLOAT_EQ(ar.xmin(), 0);
        EXPECT_FLOAT_EQ(ar.ymin(), 0);
        EXPECT_FLOAT_EQ(ar.xmax(), 0);
        EXPECT_FLOAT_EQ(ar.ymax(), 0);
        EXPECT_TRUE(ar.empty());
    }
}

TEST(BoundingBox, testIntersection2)
{
    BoundingBox base({-.5, -.5}, {.5, .5});

    {
        BoundingBox a({-1, -.5}, {0, .5});
        BoundingBox ar = base ^ a;
        EXPECT_FLOAT_EQ(ar.xmin(), -.5);
        EXPECT_FLOAT_EQ(ar.ymin(), -.5);
        EXPECT_FLOAT_EQ(ar.xmax(), 0);
        EXPECT_FLOAT_EQ(ar.ymax(), .5);
        ar = a ^ base;
        EXPECT_FLOAT_EQ(ar.xmin(), -.5);
        EXPECT_FLOAT_EQ(ar.ymin(), -.5);
        EXPECT_FLOAT_EQ(ar.xmax(), 0);
        EXPECT_FLOAT_EQ(ar.ymax(), .5);
    }
}
TEST(BoundingBox, testDifference)
{
    BoundingBox base({0, 0}, {10, 10});

    {
        BoundingBox a({5, -5}, {20, 20});
        BoundingBox ar = base - a;

        EXPECT_FLOAT_EQ(ar.xmin(), 0);
        EXPECT_FLOAT_EQ(ar.ymin(), 0);
        EXPECT_FLOAT_EQ(ar.xmax(), 5);
        EXPECT_FLOAT_EQ(ar.ymax(), 10);
    }

    {
        BoundingBox a({-10, -10}, {5, 20});
        BoundingBox ar = base - a;

        EXPECT_FLOAT_EQ(ar.xmin(), 5);
        EXPECT_FLOAT_EQ(ar.ymin(), 0);
        EXPECT_FLOAT_EQ(ar.xmax(), 10);
        EXPECT_FLOAT_EQ(ar.ymax(), 10);
    }

    {
        BoundingBox a({-10, -10}, {20, 5});
        BoundingBox ar = base - a;

        EXPECT_FLOAT_EQ(ar.xmin(), 0);
        EXPECT_FLOAT_EQ(ar.ymin(), 5);
        EXPECT_FLOAT_EQ(ar.xmax(), 10);
        EXPECT_FLOAT_EQ(ar.ymax(), 10);
    }

    {
        BoundingBox a({-10, 4}, {20, 25});
        BoundingBox ar = base - a;

        EXPECT_FLOAT_EQ(ar.xmin(), 0);
        EXPECT_FLOAT_EQ(ar.ymin(), 0);
        EXPECT_FLOAT_EQ(ar.xmax(), 10);
        EXPECT_FLOAT_EQ(ar.ymax(), 4);
    }
    {
        BoundingBox a({12, 5}, {20, 20});
        BoundingBox ar = base - a;

        EXPECT_FLOAT_EQ(ar.xmin(), 0);
        EXPECT_FLOAT_EQ(ar.ymin(), 0);
        EXPECT_FLOAT_EQ(ar.xmax(), 10);
        EXPECT_FLOAT_EQ(ar.ymax(), 10);
        EXPECT_FALSE(ar.empty());
    }
    {
        BoundingBox a({-10, -10}, {20, 20});
        BoundingBox ar = base - a;

        EXPECT_FLOAT_EQ(ar.xmin(), 0);
        EXPECT_FLOAT_EQ(ar.ymin(), 0);
        EXPECT_FLOAT_EQ(ar.xmax(), 0);
        EXPECT_FLOAT_EQ(ar.ymax(), 0);
        EXPECT_TRUE(ar.empty());
    }

}

TEST(BoudningBox, testCenter)
{

    BoundingBox a({10, 20}, {30, 100});
    Vec2f center = a.center();

    EXPECT_FLOAT_EQ(center[0], 20);
    EXPECT_FLOAT_EQ(center[1], 60);

}

TEST(BoundingBox, testAddPoint)
{
    {
        BoundingBox a({10, 20}, {30, 100});

        a.addPoint(-10, 0);
        EXPECT_FLOAT_EQ(a.xmin(), -10);
        EXPECT_FLOAT_EQ(a.ymin(), 0);

        a.addPoint(60, 100);
        EXPECT_FLOAT_EQ(a.xmax(), 60);
        EXPECT_FLOAT_EQ(a.ymax(), 100);
    }

    {
        BoundingBox a({0, 0.5}, {0, 0.5});

        a.addPoint(0.5, 0.0);
        EXPECT_FLOAT_EQ(a.xmin(), 0);
        EXPECT_FLOAT_EQ(a.ymin(), 0);
        EXPECT_FLOAT_EQ(a.xmax(), 0.5);
        EXPECT_FLOAT_EQ(a.ymax(), 0.5);

        a.addPoint(0.0, -0.5);
        EXPECT_FLOAT_EQ(a.xmin(), 0);
        EXPECT_FLOAT_EQ(a.ymin(), -0.5);
        EXPECT_FLOAT_EQ(a.xmax(), 0.5);
        EXPECT_FLOAT_EQ(a.ymax(), 0.5);

        a.addPoint(-0.5, 0.0);
        EXPECT_FLOAT_EQ(a.xmin(), -0.5);
        EXPECT_FLOAT_EQ(a.ymin(), -0.5);
        EXPECT_FLOAT_EQ(a.xmax(), 0.5);
        EXPECT_FLOAT_EQ(a.ymax(), 0.5);

    }

    
}

int main(int argc, char **argv)
{
        testing::InitGoogleTest(&argc, argv);
        setlocale(LC_CTYPE, "");
        return RUN_ALL_TESTS();
}
