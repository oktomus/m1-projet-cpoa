#include "csgTree.h"
#include "csgRegularPolygon.h"
#include "csgDisk.h"
#include "csgOperation.h"

#include <cassert>
#include <string>
#include <stdexcept>
#include <functional>

void CsgTree::addPrimitive(std::shared_ptr<CsgPrimitive> p)
{
    p->id_ = ++maxId_;

    leaves_.insert(p);
    nodes_[p->id()] = p;
    roots_.insert(p);
}

void CsgTree::clear()
{
   leaves_.clear();
   nodes_.clear();
   roots_.clear();
   maxId_ = 0;
}

int CsgTree::joinPrimitive(Operation opType, unsigned int a, unsigned int b)
{
    std::shared_ptr<CsgNode> left = nodes_[a];
    std::shared_ptr<CsgNode> right = nodes_[b];

    if(roots_.find(left) == roots_.end()) return -6;
    if(roots_.find(right) == roots_.end()) return -7;

    std::shared_ptr<CsgNode> node = std::make_shared<CsgOperation>(opType, left, right);
    node->id_ = ++maxId_;
    nodes_[node->id()] = node;
    roots_.insert(node);
    nodes_[a]->parent_= node;
    nodes_[b]->parent_= node;

    // Remove a and b from roots
    auto it = roots_.begin();
    bool dela = false, delb = false;
    while(it != roots_.end() && (!dela || !delb))
    {
        if (!dela && (*it)->id() == a)
        {
            roots_.erase(it);
            dela = true;
        }
        else if(!delb && (*it)->id() == b)
        {
            roots_.erase(it);
            delb = true;
        }
        else
            ++it;
    }
    return node->id();
}

void CsgTree::drawInImage(Image2Grey &img)
{
    static Matrix33d globalRevert;
    static unsigned int imgWidth = 0;
    static unsigned int imgHeight = 0;

    if(imgWidth != img.width || imgHeight != img.height)
    {
        imgWidth = img.width;
        imgHeight = img.height;
        Matrix33d transpos;
        transpos.translate(imgWidth/2.0, imgHeight/2.0);
        transpos.scale(imgWidth/2.0, imgWidth/2.0);
        transpos.invert(globalRevert);
    }

    #pragma omp parallel for collapse(2)
    for(size_t x = 0; x < img.width; ++x)
    {
        for(size_t y = 0; y < img.height; ++y)
        {
            Vec2f point{float(x), float(y)};
            globalRevert.apply(point);
            img(x, y) = 0;
            for(std::shared_ptr<CsgNode> node : roots_)
            {
                if(node->intersectBBox(point) && node->intersect(point))
                {
                    img(x, y) = 255;
                    break;
                }
            }
        }
    }
}

std::shared_ptr<CsgNode> CsgTree::fromId(unsigned int id)
{

   if(nodes_.find(id) == nodes_.end())
       return NULL;
    return nodes_[id];
}

unsigned int CsgTree::getMaxAvailableId()
{
    return maxId_;
}

std::string CsgTree::asciiArtGraph() const
{
   std::string res;

   for(std::shared_ptr<CsgNode> node : roots_)
   {
       res += node->asciiArtNode(0);
       res += "\n=============================================================\n";
   }
   return res;
}


void CsgTree::saveToFile(const std::string &path) const
{
    std::ofstream wf(path);
    if(!wf.is_open()) throw std::runtime_error("Can't open file for writing");
    wf << "CSG_File\n\n";

    // Keep track of written nodes
    std::vector<unsigned int> writenIds;
    // So that we know if we have to write operation's children
    // Before writing the operation
    std::function<void(std::shared_ptr<CsgNode>)> writeNode;
    writeNode = [&] (std::shared_ptr<CsgNode> a){
        if(std::find(writenIds.begin(), writenIds.end(), a->id()) == writenIds.end())
        {
            if(a->type() != "Operation") // Primitive
                a->writeTo(wf);
            else
            {
                CsgOperation * op = dynamic_cast<CsgOperation*>(a.get());
                writeNode(op->left_);
                writeNode(op->right_);
                op->writeTo(wf);
            }
            wf << "\n\n";
            writenIds.push_back(a->id());
        }

    };

    for(std::shared_ptr<CsgNode> node : roots_)
    {
        writeNode(node);
    }

    // Write roots
    wf.close();
}

void CsgTree::loadFromFile(const std::string &path)
{
    std::ifstream rf(path);
    if(!rf.is_open()) throw std::runtime_error("Can't open file for reading");

    std::string readText;
    int readInt;
    Matrix33d tr;
    std::shared_ptr<CsgPrimitive> currentNode;

    // Keep track of what have been aded to get nodes links
    std::map<unsigned int, std::shared_ptr<CsgNode>> createdNodes;

    rf >> readText;
    if(readText.compare("CSG_File") != 0)
    {
        rf.close();
        throw std::runtime_error("This is not a CSG Tree file");
    }

    while(rf >> readInt) // All nodes start with an id
    {
        // Read node
        int id= readInt;
        rf >> readText; // Type
        if(readText == "Disk") // Simpler
        {
            currentNode = std::make_shared<CsgDisk>();
            rf >> tr;
            currentNode->setTransform(tr); // MAtrix
            addPrimitive(currentNode);
            createdNodes[id] = currentNode;
        }
        else if(readText == "Polygon")
        {
            rf >> readInt; // Nb summit
            currentNode = std::make_shared<CsgRegularPolygon>(readInt);
            rf >> tr;
            currentNode->setTransform(tr); // Matrix
            addPrimitive(currentNode);
            createdNodes[id] = currentNode;
        }
        else if(readText == "Union" || readText == "Difference" || readText == "Intersection")
        {
            // Operation
            Operation opType;
            if(readText == "Union")
                opType = Union;
            else if(readText == "Intersection")
                opType = Intersection;
            else if(readText == "Difference")
                opType = Difference;

            unsigned int idLeft, idRight;
            rf >> idLeft;
            rf >> idRight;

            // Look for real new ids of childrens
            idLeft = createdNodes[idLeft]->id();
            idRight = createdNodes[idRight]->id();

            // Join
            int opId = joinPrimitive(opType, idLeft, idRight);

            // Transform
            rf >> tr;
            createdNodes[id] = nodes_[opId];
            createdNodes[id]->setTransform(tr);
        }
        else
        {
            rf.close();
            throw std::runtime_error("Unknown node type " + readText);
        }
    }

    rf.close();
}

void CsgTree::clone(const unsigned int & id)
{
    std::shared_ptr<CsgNode> top = nodes_[id];
    std::shared_ptr<CsgNode> newRoot = top->clone();

    // Recursive id update
    std::function<void(std::shared_ptr<CsgNode>)> updateId;
    updateId = [&] (std::shared_ptr<CsgNode> a){
        a->id_ = ++maxId_;
        nodes_[a->id()] = a;

        if(a->type() == "Operation")
        {
            CsgOperation* op = static_cast<CsgOperation*>(a.get());
            updateId(op->left_);
            updateId(op->right_);
        }
    };

    updateId(newRoot);
    roots_.insert(newRoot);

}

void CsgTree::unjoin(const unsigned int & id)
{
    std::shared_ptr<CsgNode> root = nodes_[id];
    if(std::find(roots_.begin(), roots_.end(), root) == roots_.end())
    {
        throw std::invalid_argument("The given node isn't a root node");
    }
    if(!root->isOperation()) return;

    CsgOperation* op = static_cast<CsgOperation*>(root.get());
    roots_.insert(op->left_);
    roots_.insert(op->right_);
    roots_.erase(roots_.find(root));
    root.reset();

    // Update ids
    std::map<unsigned int, std::shared_ptr<CsgNode>> newMap;
    for(auto node : nodes_)
    {
        if(node.first > id)
        {
            node.second->id_ -= 1;
        }
        newMap[node.second->id()] = node.second;
    }
    nodes_ = newMap;
    maxId_--;
}
