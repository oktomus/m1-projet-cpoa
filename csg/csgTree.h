#ifndef __CSG_TREE__
#define __CSG_TREE__

#include <set>
#include <map>
#include <memory>

#include "csgPrimitive.h"
#include "csgNode.h"
#include "csgOperation.h"
#include "image2D.h"

struct nodeComp {
  bool operator() (const std::shared_ptr<CsgNode>& lhs,
                   const std::shared_ptr<CsgNode>& rhs) const
  {return lhs->id()<rhs->id();}
  bool operator() (const CsgNode& lhs, const CsgNode& rhs) const
  {return lhs.id()<rhs.id();}
  bool operator() (unsigned int lhs, const CsgNode& rhs) const
  {return lhs<rhs.id();}
  bool operator() (const CsgNode& lhs, unsigned int rhs) const
  {return lhs.id()<rhs;}
};

/**
 * @brief Tree of CsgNode
 */
class CsgTree
{
public:

    /**
     * @brief Add a primitive.
     * The primitive will be a root by default.
     * @param p
     */
    void addPrimitive(std::shared_ptr<CsgPrimitive> p);

    /**
     * @brief Clear the tree
     */
    void clear();

    /**
     * @brief Join a node with another.
     * The nodes will be removes from roots if they are in
     * @param opType
     * @param a
     * @param b
     */
    int joinPrimitive(Operation opType, unsigned int a, unsigned int b);
    void drawInImage(Image2Grey & img);

    /**
     * @brief Returns the node linked to the given id
     * @param id
     * @return
     */
    std::shared_ptr<CsgNode> fromId(unsigned int id);

    /**
     * @brief Return the maximum available id in the nodes list
     * @return
     */
    unsigned int getMaxAvailableId();

    std::string asciiArtGraph() const;

    /**
     * @brief Save the tree to the given file
     * @param path
     */
    void saveToFile(const std::string& path) const;

    /**
     * @brief Append nodes in the current tree with nodes in the given file
     * @param path
     */
    void loadFromFile(const std::string& path);

    /**
     * @brief Clone the given node and add it in the tree
     */
    void clone(const unsigned int&);

    /**
     * @brief Un join the given root node. Have to be an operation and a root.
     */
    void unjoin(const unsigned int&);

private:

    std::set<std::shared_ptr<CsgPrimitive>, nodeComp> leaves_;
    std::set<std::shared_ptr<CsgNode>, nodeComp> roots_;
    std::map<unsigned int, std::shared_ptr<CsgNode>> nodes_;

    /**
     * @brief Maximum id used by the nodes.
     * The first one being 1 and not 0
     */
    unsigned int maxId_ = 0;
};

#endif
