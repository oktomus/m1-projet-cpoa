#include "csgDisk.h"

CsgDisk::CsgDisk():
    CsgPrimitive()
{
   updateBBox();
}

bool CsgDisk::intersect(const Vec2f & p) const
{
    Vec2f p2(p);
    transRevert_.apply(p2);
    return p2[0] * p2[0] + p2[1] * p2[1] <= 0.25;
}

std::string CsgDisk::asciiArtNode(int offset) const
{
   return std::string(offset, '\t') + "Disk " +
           CsgNode::asciiArtNode();
}

void CsgDisk::writeTo(std::ostream & out) const
{
    out << this->id() << " " << "Disk" << "\n";
    this->transform_.writeTo(out);
}

std::shared_ptr<CsgNode> CsgDisk::clone() const
{
   std::shared_ptr<CsgNode> cloned = std::make_shared<CsgDisk>();
   cloned->setTransform(transform_);
   return cloned;
}

void CsgDisk::updateBBox()
{
    Vec2f topCircle{0, .5f};
    Vec2f center{0, 0};

    this->transform_.apply(topCircle);
    this->transform_.apply(center);

    float radius = topCircle.distanceTo(center);

    bb_.setXmin(center[0] - radius);
    bb_.setXmax(center[0] + radius);
    bb_.setYmin(center[1] - radius);
    bb_.setYmax(center[1] + radius);
    updateParentBBox();
}
