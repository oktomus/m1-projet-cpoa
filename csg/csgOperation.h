#ifndef __CSG_OPERATION__
#define __CSG_OPERATION__

#include "csgNode.h"

#include <memory>

typedef enum{
    Intersection = 0,
    Union = 1,
    Difference = 2
} Operation;

class CsgTree;

class CsgOperation : public CsgNode
{

    friend CsgTree;

public:

    CsgOperation(Operation opType, std::shared_ptr<CsgNode> left, std::shared_ptr<CsgNode> right);

    bool intersect(const Vec2f& p) const;

    bool isOperation() const;

    void swap();

    virtual std::string asciiArtNode(int offset = 0) const;

    virtual std::string type() const;

    virtual void writeTo(std::ostream&) const;

    virtual std::shared_ptr<CsgNode> clone() const;

private:

    Operation opType_;
    std::shared_ptr<CsgNode> left_;
    std::shared_ptr<CsgNode> right_;

protected:

    void updateBBox();
};

#endif
