#ifndef __CSG_DISK__
#define __CSG_DISK__

#include "csgPrimitive.h"

class CsgDisk : public CsgPrimitive
{

public:

    CsgDisk();


    bool intersect(const Vec2f& p) const;

    virtual std::string asciiArtNode(int offset = 0) const;

    virtual void writeTo(std::ostream&) const;

    virtual std::shared_ptr<CsgNode> clone() const;

protected:

    void updateBBox();

};

#endif
