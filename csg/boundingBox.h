#ifndef __BOUNDINGBOX__
#define __BOUNDINGBOX__

#include "vector.h"

/*

 █                               █    ▀                 
 █▄▄▄    ▄▄▄   ▄   ▄  ▄ ▄▄    ▄▄▄█  ▄▄▄    ▄ ▄▄    ▄▄▄▄ 
 █▀ ▀█  █▀ ▀█  █   █  █▀  █  █▀ ▀█    █    █▀  █  █▀ ▀█ 
 █   █  █   █  █   █  █   █  █   █    █    █   █  █   █ 
 ██▄█▀  ▀█▄█▀  ▀▄▄▀█  █   █  ▀█▄██  ▄▄█▄▄  █   █  ▀█▄▀█ 
                                                   ▄  █ 
                                                    ▀▀  
 █                   
 █▄▄▄    ▄▄▄   ▄   ▄ 
 █▀ ▀█  █▀ ▀█   █▄█  
 █   █  █   █   ▄█▄  
 ██▄█▀  ▀█▄█▀  ▄▀ ▀▄ 
                     
*/

class CsgOperation;
                     
/**
  * 2D Reprensation of a bounding box axis aligned
  * AABB
  * 
  * Contains only two points, min and max
  */
class BoundingBox{

    friend CsgOperation;

public:

    /**
      * Default constructor
      *
      * @param      Bottom left corner
      * @param      Tpop right corner
      */
    BoundingBox(const Vec2f&, const Vec2f&);

    /**
     * @brief Create an empty bb
     */
    BoundingBox() : BoundingBox({0, 0}, {0, 0}) {};

    /**
      * Read lony accessors
      */
    const float& xmin() const { return xmin_; }
    const float& xmax() const { return xmax_; }
    const float& ymin() const { return ymin_; }
    const float& ymax() const { return ymax_; }

    void setXmin(const float& p) { xmin_ = p; }
    void setXmax(const float& p) { xmax_ = p; }
    void setYmin(const float& p) { ymin_ = p; }
    void setYmax(const float& p) { ymax_ = p; }

    /**
      * Expand the bounging box with the given point
      */
    template<typename T>
    void addPoint(const T&, const T&);
    void addPoint(const Vec2f&);

    /**
      * Checks if the given point is inside using a point object
      *
      * @param a    The point
      * @return     True or false
      */
    bool isPointInside(const Vec2f& a) const;
    bool isPointInside(const Vec3f& a) const;
    bool isPointInside(const float& x, const float& y) const;

    /**
      * Check if the box is empty.
      *
      * An empty box can be the intersection of two non overlapping boxes
      */
    bool empty() const;

    /**
      * Returns the center of the box
      *
      * @return     A point
      */
    Vec2f center() const;

    /**
      * Union of two Bounding box.
      *
      * @param other    The other bounding box
      */
    BoundingBox& operator += (const BoundingBox& other);
    friend BoundingBox operator+(BoundingBox a, const BoundingBox& b)
    {
        a += b;
        return a;
    }

    /**
      * Intersection of two Bounding box.
      *
      * @param other    The other bounding box
      */
    BoundingBox& operator ^= (const BoundingBox& other);
    friend BoundingBox operator^(BoundingBox a, const BoundingBox& b)
    {
        a ^= b;
        return a;
    }

    /**
      * Difference of two Bounding box.
      * Order matters.
      *
      * @param other    The other bounding box
      */
    BoundingBox& operator-= (const BoundingBox& other);
    friend BoundingBox operator-(BoundingBox a, const BoundingBox& b)
    {
        a -= b;
        return a;
    }

private:

    /**
      * Bottom left corner
      */
    float xmin_;
    float ymin_;

    /**
      * Top left corner
      */
    float xmax_;
    float ymax_;

};

template<typename T>
void BoundingBox::addPoint(const T& x, const T& y)
{
    if(xmin_ > x) xmin_ = x;
    if(xmax_ < x) xmax_ = x;
    if(ymin_ > y) ymin_ = y;
    if(ymax_ < y) ymax_ = y;
}

#endif
