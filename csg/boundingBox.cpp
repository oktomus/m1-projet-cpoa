#include "boundingBox.h"
#include <utility>
#include <cassert>

BoundingBox::BoundingBox(
        const Vec2f & bl,
        const Vec2f & tr)
{
    xmin_ = bl[0];
    ymin_ = bl[1];
    xmax_ = tr[0];
    ymax_ = tr[1];

    if(xmin_ > xmax_)
        std::swap(xmin_, xmax_);

    if(ymin_ > ymax_)
        std::swap(ymin_, ymax_);

}


void BoundingBox::addPoint(const Vec2f& p)
{
    addPoint(p[0], p[1]);
}

bool BoundingBox::isPointInside(const Vec2f& p) const
{
    return isPointInside(p[0], p[1]);
}

bool BoundingBox::isPointInside(const Vec3f &p) const
{
    return isPointInside(p[0], p[1]);
}

bool BoundingBox::isPointInside(const float &x, const float &y) const
{
    assert(!empty());
    return x >= xmin_ &&
            y >= ymin_ &&
            x <= xmax_ &&
            y <= ymax_;
}

bool BoundingBox::empty() const
{
    return xmin() >= xmax() && ymin() >= ymax();
}

Vec2f BoundingBox::center() const
{
    assert(!empty());
    return {xmax() - ((xmax() - xmin())/2),
            ymax() - ((ymax() - ymin())/2)};
}

BoundingBox& BoundingBox::operator += (const BoundingBox& other)
{
    // Top right
    if(other.xmax() > xmax()) xmax_ = other.xmax();
    if(other.ymax() > ymax()) ymax_ = other.ymax();
    // Bottom left
    if(other.xmin() < xmin()) xmin_ = other.xmin();
    if(other.ymin() < ymin()) ymin_ = other.ymin();

    return *this;
}

BoundingBox& BoundingBox::operator^=(const BoundingBox& other)
{

    bool x_overlap = 
        ( other.xmin() >= xmin() && other.xmax() <= xmax() ) ||
        ( other.xmin() <= xmin() && other.xmax() >= xmax() ) ||
        ( other.xmin() >= xmin() && other.xmax() >= xmax() &&
        other.xmin() <= xmax() ) ||
        ( other.xmin() <= xmin() && other.xmax() <= xmax() &&
        other.xmax() >= xmin() );

    bool y_overlap = 
        ( other.ymin() >= ymin() && other.ymax() <= ymax() ) ||
        ( other.ymin() <= ymin() && other.ymax() >= ymax() ) ||
        ( other.ymin() >= ymin() && other.ymax() >= ymax() &&
        other.ymin() <= ymax() ) ||
        ( other.ymin() <= ymin() && other.ymax() <= ymax() &&
        other.ymax() >= ymin() );

    if(!x_overlap || !y_overlap )
    {
        xmax_ = 0;
        ymax_ = 0;
        xmin_ = 0;
        ymin_ = 0;
        return (*this);
    }

    // Top right
    if(other.ymax() < ymax()) ymax_ = other.ymax();
    if(other.xmax() < xmax()) xmax_ = other.xmax();
    // Bottom left
    if(other.ymin() > ymin()) ymin_ = other.ymin();
    if(other.xmin() > xmin()) xmin_ = other.xmin();

    return *this;
}

BoundingBox& BoundingBox::operator-= (const BoundingBox& other)
{
    bool x_full_overlap = 
        ( other.xmin() > xmin() && other.xmax() < xmax() );
    bool x_overlap = 
        x_full_overlap ||
        ( other.xmin() < xmin() && other.xmax() > xmax() ) ||
        ( other.xmin() > xmin() && other.xmax() > xmax() && 
        other.xmin() < xmax() ) ||
        ( other.xmin() < xmin() && other.xmax() < xmax() && 
        other.xmax() > xmin() );

    bool y_full_overlap = 
        ( other.ymin() > ymin() && other.ymax() < ymax() );
    bool y_overlap = 
        y_full_overlap || 
        ( other.ymin() < ymin() && other.ymax() > ymax() ) ||
        ( other.ymin() > ymin() && other.ymax() > ymax() && 
        other.ymin() < ymax() ) ||
        ( other.ymin() < ymin() && other.ymax() < ymax() && 
        other.ymax() > ymin() );

    if(!x_overlap || !y_overlap)    {
        return (*this);
    }

    if(other.isPointInside(xmin_, ymin_) &&
            other.isPointInside(xmax_, ymax_))
    {
        xmax_ = 0;
        ymax_ = 0;
        xmin_ = 0;
        ymin_ = 0;
        return (*this);
    }

    if(other.xmax() < xmax() && other.xmax() > xmin())
    {
        xmin_ = other.xmax();
    }
    if(other.xmin() > xmin() && other.xmin() < xmax())
    {
        xmax_ = other.xmin();
    }
    if(other.ymax() > ymin() && other.ymax() < ymax())
    {
        ymin_ = other.ymax();
    }
    if(other.ymin() < ymax() && other.ymin() > ymin())
    {
        ymax_ = other.ymin();
    }


    return *this;
}
