#ifndef __CSG_PRIMITIVE__
#define __CSG_PRIMITIVE__

#include "matrix33d.h"
#include "csgNode.h"

class CsgPrimitive : public CsgNode
{

public:

    CsgPrimitive();

    virtual std::string type() const;

private:


protected:


};

#endif
