#ifndef __CSG_NODE__
#define __CSG_NODE__

#include <vector>
#include <memory>
#include "boundingBox.h"
#include "matrix33d.h"

class CsgOperation;
class CsgTree;

/**
 * @brief A node in a CSG Tree
 */
class CsgNode
{

    friend CsgOperation;
    friend CsgTree;

public:

    /**
     * @brief Create a csg node.
     * Take a care of defining a unique id.
     */
    CsgNode();

    /**
     * @brief Take care of deleting the id
     */
    ~CsgNode();

    /**
     * @brief Returns the id of the node
     * @return
     */
    unsigned int id() const;

    /**
     * @brief Returns the resulting bouding box of the node.
     * @return
     */
    const BoundingBox& getBBox() const;

    /**
     * @brief Set the transform
     * @param m
     */
    void setTransform(const Matrix33d & m);

    /**
     * @brief Returns the node transform matrix
     * @return
     */
    const Matrix33d& getTransfo() const;

    /**
     * @brief Checks if the given pixel is in the bouding box
     */
    bool intersectBBox(const Vec2f& p) const;

    virtual bool intersect(const Vec2f& p) const = 0;

    virtual bool isOperation() const;

    virtual std::string asciiArtNode(int offset=0) const;

    virtual std::string type() const = 0;

    virtual void writeTo(std::ostream&) const;

    /**
     * @brief Create a new node from the current node
     * @return
     */
    virtual std::shared_ptr<CsgNode> clone() const = 0;


private:

    /**
     * @brief The id of this node
     */
    unsigned int id_;

protected:

    /**
     * @brief The bounding box of the node
     */
    BoundingBox bb_;

    /**
     * @brief Transformation of the node
     */
    Matrix33d transform_;

    /**
     * @brief Invert of the transform matrix
     */
    Matrix33d transRevert_;

    /**
     * @brief Update the bounding box based on the node type
     */
    virtual void updateBBox() = 0;

    /**
     * @brief Update the node's parent bbox
     */
    void updateParentBBox() const;

    std::shared_ptr<CsgNode> parent_;

};

#endif
