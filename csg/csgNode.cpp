#include "csgNode.h"
#include <algorithm>

CsgNode::CsgNode()
{
}

CsgNode::~CsgNode()
{
}

unsigned int CsgNode::id() const
{
    return id_;
}

const BoundingBox& CsgNode::getBBox() const
{
    return bb_;
}

void CsgNode::setTransform(const Matrix33d &m)
{
   transform_ = m;
   transform_.invert(transRevert_);
   updateBBox();
}

const Matrix33d& CsgNode::getTransfo() const
{
    return transform_;
}

bool CsgNode::intersectBBox(const Vec2f &p) const
{
    return bb_.isPointInside(p);
}

bool CsgNode::isOperation() const
{
    return false;
}

std::string CsgNode::asciiArtNode(int offset) const
{
    return std::string("ID[") + std::to_string(id_) + "]";
}

void CsgNode::writeTo(std::ostream & out) const
{
    out << this->id_ << " " << type() << "\n";
    this->transform_.writeTo(out);
}

void CsgNode::updateParentBBox() const
{
   if(parent_.get() != NULL) parent_->updateBBox();
}
