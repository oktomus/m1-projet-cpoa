#include "csgPrimitive.h"
#include "matrix33d.h"
#include "vector.h"

CsgPrimitive::CsgPrimitive():
    CsgNode()
{
}

std::string CsgPrimitive::type() const
{
   return "Primitive";
}
