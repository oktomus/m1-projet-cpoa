cmake_minimum_required(VERSION 3.0)

project(test_particle LANGUAGES C CXX)

find_package(Threads REQUIRED)

link_directories(${GT}/lib64)

