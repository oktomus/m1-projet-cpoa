#include "particle.h"

Particle::Particle(const Vec2f & pPos, const float& pTime) :
    Particle(pPos[0], pPos[1], pTime)
{

}

Particle::Particle(const float & pX, const float & pY, const float & pTime)
{
    pos_[0] = pX;
    pos_[1] = pY;
    time_ = pTime;

}

void Particle::move()
{
    pos_ += Vec2f({1.0, 1.0});
    time_ += 1.0;
}

const float &Particle::getTime() const
{
    return time_;
}

bool operator<(const Particle & left, const Particle & right)
{
    return left.time_ < right.time_;
}
