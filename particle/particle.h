#ifndef __PARTICLE__
#define __PARTICLE__

#include "vector.h"

class RenderImg;

/**
 * @brief A particule
 */
class Particle
{
    friend RenderImg;
public:

    Particle(const Vec2f&, const float&);
    Particle(const float&, const float&, const float&);

    void move();

    friend bool operator<(const Particle&, const Particle&);

    const float& getTime() const;

private:

    Vec2f pos_;
    float time_;

};

#endif
