# Projet CPOA: CSG

-------------------------------

- **Git**: [https://git.unistra.fr/kmasson/M1ISI_CPOA_MASSON](https://git.unistra.fr/kmasson/M1ISI_CPOA_MASSON)
- **Kevin Masson**, 2017
- M1 ISI S1

-------------------------------

## Documentation

Disponible dans `doc/html`.

    $ firefox doc/html/index.html

## Compilation

    # A la racine du projet
    $ sh install_google_test.sh
    $ cmake .
    $ make

Via QT, rien de spécial à faire en plus.

## Lancer les tests

Liste des tests:
- *vector/tests/test_array*
- *vector/tests/test_vector*
- *vector/tests/test_matrix33d*
- *image/tests/test_image2D*
- *particle/tests/test_boundingBox*

**Attention !** Le mieux est de les lancer depuis la racine du projet via le Terminal. 
Pour les lancers depuis QT, il faut spécifier pour le test `test_image2D` le chemin vers les fichiers de tests. C'est le dossier `test_files` dans le dossier build. Ce dossier contient des images utilisées pour les tests de lecture et écriture.

    $ ./image/tests/test_image2D

## Ajouts à l'interface

Il est possible de faire un seuil, un sous échantillionage ou un lissage directement depuis l'interface dans le menu image.
